#! /bin/bash
#
#	ci_build.sh
#	build saracrypt (do not install)
#
#	* default prefix is /usr/local
#	* define prefix in setup.conf
#	* may also be invoked as:
#
#	    SETUP_CONF=mysetup.conf ./ci_build.sh
#

# rudimentary check for source files
if [[ ! -e saracrypt.1.ronn ]]
then
	echo "setup.sh: error: missing source files; run setup.sh from source dir"
	exit 1
fi

if [[ -z $SETUP_CONF ]]
then
	# detect platform : $ID will have OS name
	if [[ -e /etc/os-release ]]
	then
		. /etc/os-release

		# CentOS|Fedora fallback to RHEL
		if [[ $ID == centos && ! -e setup.conf.centos ]]
		then
			ID=rhel
		fi
		if [[ $ID == fedora && ! -e setup.conf.fedora ]]
		then
			ID=rhel
		fi

		# Ubuntu|Mint fallback to debian
		if [[ "$ID" == ubuntu && ! -e setup.conf.ubuntu ]]
		then
			ID=debian
		fi
		if [[ $ID == mint && ! -e setup.conf.mint ]]
		then
			ID=debian
		fi

	else
		ID=$(uname |tr '[:upper:]' '[:lower:]')
		if [[ $ID == darwin ]]
		then
			ID=mac
		fi
	fi

	# load appropriate setup.conf for this platform
	if [[ ! -e setup.conf.$ID ]]
	then
		echo "setup.sh: using generic setup.conf"
		SETUP_CONF=setup.conf
	else
		echo "setup.sh: using setup.conf.$ID"
		SETUP_CONF=setup.conf.$ID
	fi
fi

# check for setup config file
if [[ ! -e $SETUP_CONF ]]
then
	echo "setup.sh: error: file $SETUP_CONF not found"
	exit 255
fi

# load the setup config file
. $SETUP_CONF


check_dir() {
	# check that directory "$1" exists
	# exit on error

	if [[ ! -d $1 ]]
	then
		echo "setup.sh: error: directory $1 does not exist"
		exit 255
	fi
}

# check that system directories exist
# and bail out if they do not exist
# we will not create any system directories if they do not exist
check_dir "$PREFIX"
check_dir "$BINDIR"
check_dir "$LIBDIR"
check_dir "$MANDIR"
check_dir "$MANDIR/man1"
check_dir "$DOCDIR"


# generate manpage saracrypt.1
RONN=${RONN:-$(command -v ronn)}
if [[ ! -z $RONN ]]
then
	$RONN --organization SURFsara -r saracrypt.1.ronn
fi

# generate userguide.html documentation
# if we have markdown
MARKDOWN=${MARKDOWN:-$(command -v markdown)}
if [[ ! -z $MARKDOWN ]]
then
	# use smartypants (if we have it)
	SMARTYPANTS=${SMARTYPANTS:-$(command -v smartypants)}
	if [[ ! -z $SMARTYPANTS ]]
	then
		$MARKDOWN userguide.md |$SMARTYPANTS >userguide.html
	else
		$MARKDOWN userguide.md >userguide.html
	fi
fi

# path of python3
PYTHON3=${PYTHON3:-$(command -v python3)}
if [[ -z $PYTHON3 ]]
then
	echo "error: python3 not found"
	exit 1
fi

# these vars need to be exported; used by Makefile
export MAKEFILE
export CC SWIG PYTHON3 RM INSTALL
export CFLAGS LFLAGS LIBS
export PYTHON_INCLUDE

( cd src && make -f $MAKEFILE clean && make -f $MAKEFILE )
if (( $? != 0 ))
then
	echo "There were build errors. setup aborted"
	exit 255
fi

# EOB
