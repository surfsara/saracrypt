#! /usr/bin/env python3
#
#   saracrypt   WJ118
#   Copyright 2018 SURFsara B.V.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''(bulk) file encryption tool

Classes in this code:

  ArgsParser - ArgumentParser with nicer help text
  Cryptor - encryption/decryption engine based on GPG
  Metric - handle values with kB/MB/GB or kiB/MiB/GiB, etc.
  ProgressBar - generic ASCII progress bar (used by Cryptor)
  Masterkey - masterkey implementation (uses Cryptor)
  MyDirEntry - pseudo-DirEntry object for file stat information
  BatchPath - file path structure (used by Batch)
  Batch - bulk filelists (includes dmput/dmget, uses BatchPath, MyDirEntry)
  BatchCrypt - bulk encrypt/decrypt filelist
  BatchDelete - bulk delete filelist

Custom exceptions:

  BatchError - error(s) while handling a Batch
  BatchFullError - 'internal' exception for signalling a full Batch
'''

import os
import sys
import argparse
import subprocess
import errno
import shlex
import stat
import fnmatch
import getpass
import time
import resource
import inspect

from typing import List, Dict, Generator, Union, Sequence, Tuple, Optional

# s2k comes from PYTHONPATH
import s2k

VERSION = '1.0'
#PROG = os.path.basename(sys.argv[0])
PROG = 'saracrypt'

DEBUG_COLORS = True


def debug_msg(msg: str) -> None:
    '''print message in debug mode'''

    frames = inspect.stack()
    if DEBUG_COLORS:
        sys.stdout.write('\x1b[32m')
    sys.stdout.write('% {}(): '.format(frames[1][3]))
    if DEBUG_COLORS:
        sys.stdout.write('\x1b[0m')
    print(msg)


def debug(_msg: str) -> None:
    '''default: disabled debug function'''
    #pass


def error(msg: str) -> None:
    '''print error message'''

    sys.stdout.flush()
    sys.stderr.write(f'error: {msg}\n')
    sys.stderr.flush()


def warning(msg: str) -> None:
    '''print warning message'''

    sys.stdout.flush()
    sys.stderr.write(f'warning: {msg}\n')
    sys.stderr.flush()


def info(msg: str) -> None:
    '''print informational message'''

    print(msg)


def fatal_error(msg: str) -> None:
    '''print error message and exit'''

    sys.stdout.flush()
    sys.stderr.write(f'error: {msg}\n')
    sys.stderr.flush()
    sys.exit(-1)


def suppress_output(_msg: str) -> None:
    '''do not print any message'''

    # info() and warning() are redirected here
    # when option --quiet is set
    #pass


def print_progress(filename: str) -> None:
    '''print filename'''

    # only used to print filename being crypted
    # this function is suppressed when --quiet and --batch are set

    print(filename)


def is_exec(path: str) -> bool:
    '''Returns True if path is an executable file'''

    return os.path.isfile(path) and os.access(path, os.X_OK)


def is_opendir(path: str) -> bool:
    '''Returns True if path is accessible by group/others
    May raise OSError
    '''

    if not path:
        # the dirname of a relative path is empty
        return True

    if path == '/':
        return True

    statbuf = os.stat(path)
    if statbuf.st_mode & (stat.S_IXGRP | stat.S_IXOTH):
        # this dir is open; is the parent dir open?
        return is_opendir(os.path.dirname(path))

    # dir is not open
    return False


def check_file_permissions(filename: str) -> None:
    '''Produces a warning message if the permissions are too open
    May raise OSError
    '''

    # if the permission bits on the file are all set, but
    # the directory access is closed, then the file is still safe

    statbuf = os.stat(filename)
    if statbuf.st_mode & (stat.S_IRGRP | stat.S_IROTH):
        # file is readable by group/others, go check whether
        # the directories above are accessible (x-bit set)

        if os.path.isabs(filename):
            fullpath = filename
        else:
            # if relative path, put cwd in front
            fullpath = os.path.join(os.getcwd(), filename)
        if is_opendir(os.path.dirname(fullpath)):
            warning(f'{filename}: file is readable by group and/or others')


def split_cmd(cmd: str, myvars: Union[Dict[str, str], Dict[str, Sequence[str]]] = None) -> List[str]:
    '''split command into command array'''

    if myvars is None:
        return shlex.split(cmd)

    cmd_arr: List[str] = []
    for elem in shlex.split(cmd):
        # expand variables
        if elem and elem[0] == '$':
            assert elem in myvars
            value = myvars[elem]
            if isinstance(value, list):
                cmd_arr.extend(value)
            else:
                cmd_arr.append(value)   # type: ignore
        else:
            cmd_arr.append(elem)

    return cmd_arr


def enter_password(verify: bool = False, extra_prompt: str = '') -> str:
    '''Prompts for passphrase
    Returns the password
    Raises ValueError if any problems with passphrase
    '''

    if extra_prompt:
        prompt = f'Enter {extra_prompt} passphrase: '
    else:
        prompt = 'Enter passphrase: '

    password = getpass.getpass(prompt)
    if not password:
        raise ValueError('empty passphrase')

    if verify:
        password2 = getpass.getpass('Enter it again (for verification): ')
        if password != password2:
            raise ValueError('passphrases do not match')

    return password


def load_password(filename: str) -> str:
    '''load password from file
    Returns the loaded password
    Raises ValueError if empty passphrase
    Raises OSError if problems with file
    '''

    check_file_permissions(filename)

    line = None
    f = open(filename)
    with f:
        line = f.readline()

    if not line:
        raise ValueError(f'{filename}: empty passphrase')

    password = line.rstrip(os.linesep)
    if not password:
        raise ValueError(f'{filename}: empty passphrase')

    return password


class Cryptor:
    '''represents an encryption engine, in this case powered by GnuPG'''

    DEFAULT_GPG = 'gpg'
    DEFAULT_CIPHER = 'AES256'
    SUFFIX = '.gpg'
    SUPPORTED_MAJOR_VERSIONS = ('2',)
    GNUPG_DIR = '~/.gnupg'
    RANDOM_SEED = '~/.gnupg/random_seed'

    CMD_ENCRYPT_FILE = ('$CMD_GPG --symmetric --s2k-mode 3 --s2k-count $S2K_COUNT --cipher-algo $CIPHER '
                        '--compress-algo none --batch --passphrase-fd 0 --status-fd 1 '
                        '--enable-progress-filter --quiet --output $OUT_FILENAME $IN_FILENAME')
    CMD_DECRYPT_FILE = ('$CMD_GPG --batch --yes --passphrase-fd 0 --status-fd 1 --enable-progress-filter '
                        '--output $OUT_FILENAME --quiet --decrypt $IN_FILENAME')
    CMD_ENCRYPT_STDIN = ('$CMD_GPG --symmetric --s2k-mode 3 --s2k-count $S2K_COUNT --cipher-algo $CIPHER '
                         '--compress-algo none --batch --passphrase-fd 0 --quiet --output $OUT_FILENAME')
    CMD_DECRYPT_STDOUT = '$CMD_GPG --batch --yes --passphrase-fd 0 --quiet --decrypt $IN_FILENAME'
    CMD_LIST_ALGORITHMS = '$CMD_GPG --version'

    def __init__(self, cmd_gpg: str = None, dryrun: bool = False) -> None:
        '''initialize instance
        May raise RuntimeError if the gpg command is not behaving as expected
        '''

        if cmd_gpg is None:
            cmd_gpg = Cryptor.DEFAULT_GPG
        self.cmd_gpg = Cryptor.locate_gpg(cmd_gpg)
        self.dryrun = dryrun
        # get list of supported ciphers
        # this also performs a version check along the way
        try:
            self.all_ciphers = Cryptor.get_supported_ciphers(self.cmd_gpg)
        except OSError:
            # failed to get ciphers
            self.all_ciphers = []
        if not self.all_ciphers:
            raise RuntimeError('''got empty list of supported ciphers, gpg command not behaving as expected''')
        # determine the s2k count for current machine
        self.s2k_count = s2k.get_s2k_count()
        debug(f's2k_count == {self.s2k_count}')
        if self.s2k_count < 65536:  # invalid value?
            self.s2k_count = 65536
            debug(f'invalid s2k_count; adjusted to {self.s2k_count}')

    @staticmethod
    def locate_gpg(cmd: str) -> str:
        '''check that the GPG program is installed
        Returns path of the GPG executable
        May raise ValueError, OSError
        '''

        if os.path.sep in cmd:
            # it is an absolute or relative path
            if not is_exec(cmd):
                if not os.path.exists(cmd):
                    raise ValueError(f'{cmd}: no such executable')
                #else:
                raise ValueError(f'{cmd}: not an executable program')
            # ok
            debug(f'using gpg command: {cmd}')
            return cmd

        # try finding it on PATH
        try:
            path = os.environ['PATH']
        except KeyError as err:
            debug('PATH environment variable is not set')
            raise ValueError(f'executable {cmd} not found on PATH') from err

        for elem in path.split(os.pathsep):
            fullpath = os.path.join(elem, cmd)
            if is_exec(fullpath):
                debug(f'using gpg command: {fullpath}')
                return fullpath

        raise ValueError(f'executable {cmd} not found on PATH')

    @staticmethod
    def version_check(line: str) -> None:
        '''check that the version of GPG is supported
        Displays a warning if not OK
        '''

        arr = line.split()
        try:
            if not arr:
                debug('got empty list')
                raise ValueError('unable to check gpg version number')

            if arr[0] != 'gpg':
                debug(f"program name '{arr[0]}' != 'gpg'")
                raise ValueError('unable to check gpg version number')

            version_string = arr[-1]
            debug(f'version_string: {version_string}')
            if '.' not in version_string:
                raise ValueError('unable to check gpg version number')

            version_digits = version_string.split('.')
            if version_digits[0] not in Cryptor.SUPPORTED_MAJOR_VERSIONS:
                raise ValueError('this version of gpg is not supported')

        except ValueError as err:
            warning(f'{err}')

    @staticmethod
    def get_supported_ciphers(cmd_gpg: str = None) -> List[str]:
        '''Returns list of supported ciphers
        Returns empty list if it fails to find any
        May raise OSError
        '''

        try:
            if cmd_gpg is None:
                cmd_gpg = Cryptor.DEFAULT_GPG
            cmd_gpg = Cryptor.locate_gpg(cmd_gpg)
        except ValueError as err:
            error(f'{err}')
            return []

        # run gpg --version
        cmd_arr = split_cmd(Cryptor.CMD_LIST_ALGORITHMS, {'$CMD_GPG': cmd_gpg})
        cmd = ' '.join(cmd_arr)
        debug(f'exec: {cmd}')
        try:
            proc = subprocess.run(cmd_arr, stdout=subprocess.PIPE, universal_newlines=True,
                                  encoding='utf-8', shell=False, check=True)
        except OSError as err:
            error(f'{cmd_arr[0]}: {err.strerror}')
            return []
        except subprocess.CalledProcessError as err:
            error(f'{cmd_arr[0]} exited with returncode {err.returncode}')
            return []

        # parse the output : get list of supported ciphers
        lines = proc.stdout.split(os.linesep)
        if not lines:
            debug('no output from gpg command (?)')
            return []

        cipher_line = ''
        more_lines = False

        # version should be listed in the first line of output
        Cryptor.version_check(lines[0])

        for line in lines:
            if line.startswith('Cipher: '):
                cipher_line = line[8:]
                more_lines = True

            elif line.startswith(' ') and more_lines:
                cipher_line += line.strip()

            elif more_lines:
                more_lines = False

        cipher_line = cipher_line.upper()
        debug(f'cipher_line == [{cipher_line}]')
        cipher_line = cipher_line.replace(',', ' ')
        cipher_list = cipher_line.split()
        debug('ciphers == {}'.format(str(cipher_list)))
        return cipher_list

    def encrypt_file(self, filename: str, output_filename: str, password: str,
                     cipher: str = None, display_progressbar: bool = False) -> None:
        '''encrypt a single file using password
        It expects the input file to be named without extension .gpg
        and the output filename to have extension .gpg
        May raise FileNotFoundError, FileExistsError, subprocess.CalledProcessError, ValueError, OSError
        '''

        assert filename is not None
        assert output_filename is not None
        assert password is not None

        if not password:
            raise ValueError('empty password')

        if cipher is None:
            cipher = Cryptor.DEFAULT_CIPHER
        if cipher not in self.all_ciphers:
            raise ValueError(f'''unsupported cipher: {cipher}''')

        if filename.endswith(Cryptor.SUFFIX):
            raise FileExistsError(errno.EEXIST, 'file is already encrypted', filename)

        if not output_filename.endswith(Cryptor.SUFFIX):
            raise ValueError(f'output filename {output_filename} does not end with {Cryptor.SUFFIX}')

        if os.path.exists(output_filename):
            raise FileExistsError(errno.EEXIST, f'encrypted file {output_filename} already exists', filename)

        output_dir = os.path.dirname(output_filename)
        if output_dir:
            os.makedirs(output_dir, exist_ok=True)

        self._gpg_file(filename, output_filename, password, cipher, decrypt=False, display_progressbar=display_progressbar)

    def decrypt_file(self, filename: str, output_filename: str, password: str,
                     display_progressbar: bool = False) -> None:
        '''decrypt a file using password
        It expects the input file to be named with extension .gpg
        and the output filename without .gpg
        May raise FileNotFoundError, FileExistsError, subprocess.CalledProcessError, ValueError, OSError
        '''

        assert filename is not None
        assert output_filename is not None
        assert password is not None

        if not filename.endswith(Cryptor.SUFFIX):
            raise FileExistsError(errno.EEXIST, 'file is not encrypted', filename)

        if output_filename.endswith(Cryptor.SUFFIX):
            raise ValueError(f'output filename {output_filename} should not have extension {Cryptor.SUFFIX}')

        if os.path.exists(output_filename):
            raise FileExistsError(errno.EEXIST, f'decrypted file {output_filename} already exists', filename)

        output_dir = os.path.dirname(output_filename)
        if output_dir:
            os.makedirs(output_dir, exist_ok=True)

        self._gpg_file(filename, output_filename, password, decrypt=True, display_progressbar=display_progressbar)

    def _gpg_file(self, in_filename: str, out_filename: str, password: str,
                  cipher: str = None, decrypt: bool = False, display_progressbar: bool = False) -> None:
        '''run GPG to crypt a file, optionally with a progressbar
        For decryption, parameter cipher is not used and may be None

        May raise FileNotFoundError, OSError
        '''

        progressbar: Optional[ProgressBar] = None
        if display_progressbar:
            progressbar = ProgressBar(out_filename)
            progressbar.show()
        else:
            print_progress(f'{out_filename}')

        if decrypt:
            cmd = Cryptor.CMD_DECRYPT_FILE
        else:
            cmd = Cryptor.CMD_ENCRYPT_FILE

        cmd_dict = {'$CMD_GPG': self.cmd_gpg,
                    '$IN_FILENAME': in_filename,
                    '$OUT_FILENAME': out_filename}
        if not decrypt:
            assert cipher is not None
            cmd_dict['$CIPHER'] = cipher
            cmd_dict['$S2K_COUNT'] = f'{self.s2k_count}'

        try:
            cmd_arr = split_cmd(cmd, cmd_dict)
            debug('exec: {}'.format(' '.join(cmd_arr)))
            if not self.dryrun:
                # Note: do not redirect stderr; let gpg write error messages to stderr
                proc = subprocess.Popen(cmd_arr, bufsize=-1, stdin=subprocess.PIPE,
                                        stdout=subprocess.PIPE, shell=False)
                with proc:
                    assert proc.stdin is not None                       # this helps mypy
                    assert proc.stdout is not None                      # this helps mypy

                    proc.stdin.write(bytes(password.encode()))
                    proc.stdin.flush()
                    proc.stdin.close()
                    while True:
                        data = proc.stdout.readline()
                        if not data:
                            break

                        # read the status output so that we can make
                        # a progress bar for GPG
                        line = data.decode('utf-8')
                        line = line.rstrip()
                        debug(f'line == {line}')
                        if line.startswith('[GNUPG:]'):
                            if progressbar is not None and line.startswith('[GNUPG:] PROGRESS '):
                                try:
                                    value, max_value = Cryptor._gpg_progress_from_line(line)
                                    progressbar.max_value = max_value
                                    progressbar.update(value)
                                except ValueError:
                                    # error in gpg status line
                                    pass
                        else:
                            # relay message to stdout
                            print(line)

                    proc.wait()
                    if proc.returncode != 0:
                        # non-zero exit, raise exception
                        raise subprocess.CalledProcessError(proc.returncode, cmd_arr)

        except KeyboardInterrupt:
            # delete half-finished output file
            debug(f'Ctrl-C pressed; deleting unfinished output {out_filename}')
            try:
                os.unlink(out_filename)
            except FileNotFoundError:
                pass
            except OSError as err:
                error(f'failed to remove half-finished output {out_filename}: {err.strerror}')

            # re-raise KeyboardInterrupt to higher level
            raise

        finally:
            if progressbar is not None:
                progressbar.finish()

    @staticmethod
    def _gpg_progress_from_line(line: str) -> Tuple[int, int]:
        '''Parse GPG status line
        Returns two numbers: (value, max_value)
        which should represent the progress in bytes

        Note that gpg may round to kiB or MiB so it may happen that
        the max_value is not exactly equal to the filesize
        (This depends on the version of gpg)

        May raise ValueError
        '''

        # obviously, this only works for GPG

        debug(f'gpg status: {line}')
        try:
            arr = line.split()
            if not arr:
                raise ValueError('error in gpg status line')

            debug(f'arr[1] == {arr[1]}')
            if arr[1] != 'PROGRESS':
                raise ValueError('error in gpg status line')

            # some versions of GPG give the progress numbers in bytes,
            # some versions put a metric at the end of the line
            # and will report B/KiB/MiB (and possibly even larger?)
            # so parse the metric and use it as multiplier

            debug(f'arr[-1] == {arr[-1]}')
            multiplier = 1
            if arr[-1][-1] == 'B':
                metric = arr.pop()
                multiplier = Metric.multiplier(metric)

            # only respond to a true progress line update
            if arr[-3] != '?':
                raise ValueError('error in gpg status line')

            # return number of bytes done
            # (this may raise ValueError)
            value = int(arr[-2]) * multiplier
            max_value = int(arr[-1]) * multiplier
            debug(f'return value: {value}, {max_value}')
            return value, max_value

        except IndexError as err:
            debug('caught IndexError; raising ValueError')
            raise ValueError('error in gpg status line') from err

    def encrypt_data(self, data: str, out_filename: str, password: str, cipher: str = None) -> None:
        '''Encrypt data into filename
        Writes output out_filename
        May raise ValueError, OSError, subprocess.CalledProcessError
        '''

        assert password is not None
        if not password:
            raise ValueError('empty password')

        if cipher is None:
            cipher = Cryptor.DEFAULT_CIPHER
        if cipher not in self.all_ciphers:
            raise ValueError(f'''unsupported cipher: {cipher}''')

        if os.path.exists(out_filename):
            raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), out_filename)

        data = password + os.linesep + data

        cmd_arr = split_cmd(Cryptor.CMD_ENCRYPT_STDIN,
                            {'$CMD_GPG': self.cmd_gpg,
                             '$CIPHER': cipher,
                             '$S2K_COUNT': f'{self.s2k_count}',
                             '$OUT_FILENAME': out_filename})
        debug('exec: {}'.format(' '.join(cmd_arr)))
        try:
            if not self.dryrun:
                # Note: do not redirect stderr; let gpg write error messages to stderr
                subprocess.run(cmd_arr, input=data, check=True, encoding='utf-8',
                               universal_newlines=True, shell=False)
        except (subprocess.CalledProcessError, KeyboardInterrupt):
            debug(f'an error occurred, deleting invalid output {out_filename}')
            try:
                # delete invalid outcome
                os.unlink(out_filename)
            except FileNotFoundError:
                pass
            except OSError as err:
                error(f'failed to remove half-finished output {out_filename}: {err.strerror}')

            # re-raise exception to higher level
            raise

    def decrypt_data(self, filename: str, password: str, maxsize: int = 4096) -> str:
        '''decrypt file using password and return unencrypted data
        The unencrypted data does not touch the disk. Only files smaller than maxsize (4 kiB)
        are supported
        May raise ValueError, OSError, subprocess.CalledProcessError
        '''

        assert password is not None
        # password may be empty

        if not filename.endswith(Cryptor.SUFFIX):
            raise FileExistsError(errno.EEXIST, 'file is not encrypted', filename)

        # check filesize (file can not be larger than ...)
        if os.path.getsize(filename) > maxsize:
            raise ValueError(f'{filename}: file is too large')

        data = password + os.linesep

        # decrypt to stdout
        # Note that we don't explicitly name a cipher; any will do
        cmd_arr = split_cmd(Cryptor.CMD_DECRYPT_STDOUT,
                            {'$CMD_GPG': self.cmd_gpg,
                             '$IN_FILENAME': filename})
        debug('exec: {}'.format(' '.join(cmd_arr)))
        # no dryrun here ... because output is not written to disk
        proc = subprocess.run(cmd_arr, input=data, stdout=subprocess.PIPE,
                              shell=False, check=True, universal_newlines=True, encoding='utf-8')
        return proc.stdout

    @staticmethod
    def check_gnupg_dir() -> None:
        '''make sure that the ~/.gnupg dir exists and is user-writeable
        May raise OSError
        '''

        # Note: some old version of gpg actually exists that creates a
        # read-only ~/.gnupg and consequently fails to write to it (?!!)

        gnupg_dir = os.path.expanduser(Cryptor.GNUPG_DIR)
        try:
            os.mkdir(gnupg_dir, 0o700)
        except OSError as err:
            if err.errno != errno.EEXIST:
                raise err

            # already exists, but check that it's a dir and
            # report 'Not a directory' if it isn't a dir
            if not os.path.isdir(gnupg_dir):
                raise NotADirectoryError(errno.ENOTDIR, os.strerror(errno.ENOTDIR), gnupg_dir) from err
        else:
            debug(f'mkdir -m 0700 {gnupg_dir}')

        # dir exists, just force mode 0700
        mode = stat.S_IMODE(os.stat(gnupg_dir).st_mode)
        if mode != 0o700:
            debug(f'chmod 0700 {gnupg_dir}')
            os.chmod(gnupg_dir, 0o700)

        # if random_seed exists, it should have mode 0600
        random_seed = os.path.expanduser(Cryptor.RANDOM_SEED)
        try:
            os.chmod(random_seed, 0o600)
        except OSError:
            # don't care
            pass



class Metric:
    '''handle values like kB/MB/GB or kiB/MiB/GiB, etc.'''

    # values greater than SMALL_NUMBER will be formatted as
    # a fraction of the next multiplier; e.g. "0.9 GB"
    SMALL_NUMBER = 850

    def __init__(self, value: int = 0) -> None:
        '''initialize instance'''

        self.value = value

    @staticmethod
    def multiplier(svalue: str) -> int:
        '''Parse metric like "B", "kiB", "MiB", "GB", etc.
        Returns the multiplier value
        Raises ValueError for invalid metric passed
        '''

        if svalue is None or not svalue:
            # assume bytes
            return 1

        orig = svalue
        svalue = svalue.upper()

        if svalue[-1] == 'B':
            svalue = svalue[:-1]
            if not svalue:
                # it is in bytes
                return 1

        # we use uppercase, so check for capital 'I'
        if svalue[-1] == 'I':
            factor = 1024
            svalue = svalue[:-1]
        else:
            factor = 1000

        if len(svalue) != 1:
            raise ValueError(f'invalid metric: {orig}')

        # we use uppercase, so note the capital 'K' in here
        si_prefix = ' KMGTPEZY'
        n = si_prefix.index(svalue)
        return factor ** n

    def parse(self, svalue: str) -> int:
        '''Parse formatted string with kB/MB/GB into integer
        and return that integer value
        '''

        if not svalue:
            raise ValueError(f"invalid value: '{svalue}'")

        parse_value = svalue.lower()
        if len(parse_value) >= 2 and (parse_value[-2:] == 'ps' or parse_value[-2:] == '/s'):
            parse_value = parse_value[:-2]

        arr = parse_value.split()
        if len(arr) == 1:
            # assume bytes
            if '.' in arr[0] or ',' in arr[0]:
                # it should have been an integer
                raise ValueError(f"invalid value: '{svalue}'")
            self.value = int(arr[0])
            return self.value

        if len(arr) > 2:
            raise ValueError(f"invalid value: '{svalue}'")

        value = float(arr[0])
        multiplier = Metric.multiplier(arr[1])
        self.value = int(value * multiplier)
        return self.value

    def format(self, factor: int = 1000) -> str:
        '''Return value formatted as string
        Default factor is 1000; pass factor=1024 for kiB, MiB, GiB
        '''

        if factor == 1000:
            suffix = 'B'
        elif factor == 1024:
            if self.value < Metric.SMALL_NUMBER:
                suffix = 'B'
            else:
                suffix = 'iB'
        else:
            raise RuntimeError('invalid factor: %d' % factor)
        return Metric.prefix(self.value, factor) + suffix

    @staticmethod
    def prefix(n: int, factor: int = 1000) -> str:
        '''Returns string that shows number n
        with SI prefix appended
        '''

        # hack for small numbers; the loop below will always
        # print a floating point number
        if n < Metric.SMALL_NUMBER:
            return f'{n} '

        for x, a_prefix in reversed(list(enumerate(' kMGTPEZY'))):
            big_x = factor ** x
            if n >= big_x:
                fvalue = n / float(big_x)
                return f'{fvalue:.1f} {a_prefix}'

            fraction = n / float(big_x)
            rest = fraction - int(fraction)
            if rest >= 0.85:
                return f'{fraction:.1f} {a_prefix}'
        # not reached, but hey
        return f'{n} '

    def __str__(self) -> str:
        '''Return value as string'''

        return self.format()



class ProgressBar:
    '''implements a generic progress bar'''

    WIDTH = 15
    CHARSET = '|= |'
    UPDATE_FREQUENCY = 3   # times per second

    def __init__(self, label: str = '', max_value: int = 0, start_value: int = 0, clear: bool = False) -> None:
        '''initialize instance'''

        self.max_value = max_value
        self.label = label
        self.value = start_value
        self.clear = clear
        try:
            self._percent = int(self.value / self.max_value * 100.0 + 0.5)
        except ZeroDivisionError:
            self._percent = 0
        self._lastupdate = 0.0
        # look at TERM environment variable
        # if it is not set to 'dumb', assume we can do vt100
        try:
            self.dumb_terminal = (os.environ['TERM'] == 'dumb')
        except KeyError:
            # $TERM is not set
            self.dumb_terminal = True
        debug(f'dumb_terminal == {self.dumb_terminal}')

    def show(self) -> None:
        '''display the bar'''

        if self.label:
            print(self.label, end=' ')

        print(str(self), end='', flush=True)
        self._lastupdate = time.monotonic()

    def erase(self) -> None:
        '''erase the display of the bar'''

        # erase only the bar, not the label
        if self.dumb_terminal:
            print('\b \b' * len(str(self)), end='')
        else:
            # VT100 move cursor back + clear to eol
            print(f'\x1b[{len(str(self))}D\x1b[K', end='')

    def update(self, value: int) -> None:
        '''update value and re-display bar'''

        if value == self.value:
            # no change
            return

        if value <= 0:
            percent = 0
        elif value >= self.max_value:
            percent = 100
        else:
            try:
                percent = int(value / self.max_value * 100.0 + 0.5)
            except ZeroDivisionError:
                percent = 0

        if percent == self._percent:
            # no visible change
            return

        # check time since last update
        clock_now = time.monotonic()
        freq = 1.0 / self.UPDATE_FREQUENCY
        if clock_now - self._lastupdate < freq:
            # called too often
            return

        self.erase()
        self.value = value
        self._percent = percent
        print(str(self), end='', flush=True)
        self._lastupdate = clock_now

    def finish(self) -> None:
        '''end the progress bar; erases display'''

        if self.clear and self.label:
            # clear bar + label
            if self.dumb_terminal:
                print('\r' + ' ' * (len(self.label) + 1 + len(str(self))) + '\r')
            else:
                # VT100 clear to eol
                print('\r\x1b[K')
        else:
            # only clear the bar; label stays
            self.erase()
            print('')

    def __str__(self) -> str:
        '''Returns string; what the ASCII progressbar looks like'''

        dots = int(self.WIDTH / 100.0 * self._percent + 0.5)
        return '{}{}{}{} {:3}% '.format(self.CHARSET[0],
                                        self.CHARSET[1] * dots,
                                        self.CHARSET[2] * (self.WIDTH - dots),
                                        self.CHARSET[3],
                                        self._percent)



class Masterkey:
    '''represents a masterkey'''

    # masterkeys look like (for example): "VXZF-1EDG-I16B-YUVH-5OBO"
    KEY_GROUPLEN = 4
    KEY_GROUPS = 5
    KEY_LEN = KEY_GROUPLEN * KEY_GROUPS
    ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789'
    CIPHER = 'AES256'

    def __init__(self, filename: str, cryptor: Cryptor, dryrun: bool = False) -> None:
        '''initialize instance'''

        assert filename
        self.filename = filename
        if not self.filename.endswith(Cryptor.SUFFIX):
            # append suffix
            self.filename += Cryptor.SUFFIX
        self.cryptor = cryptor
        self.dryrun = dryrun
        # Note: I would have preferred to put self.key = None
        # but mypy really doesn't like it, and even Optional[str]
        # gives problems down the line
        # So now we have a workaround for mypy here..? Not happy
        self.key = ''

    def generate(self) -> None:
        '''Generate new masterkey file
        May raise FileExistsError, subprocess.CalledProcessError
        '''

        key_bytes = os.urandom(Masterkey.KEY_LEN)
        debug(f'key_bytes == [{key_bytes!r}]')
        # this creates a key such as: "VXZF-1EDG-I16B-YUVH-5OBO"
        key = ''
        for n, k in enumerate(key_bytes):
            if n and n % Masterkey.KEY_GROUPLEN == 0:
                key += '-'
            key += Masterkey.ALPHABET[k % len(Masterkey.ALPHABET)]

        self.key = key
        debug(f'key == [{self.key}]')

        # write the encrypted masterkey file
        self.save()

        print(f'''
The master recovery key is:

        {self.key}

You should print this key out on paper and store it in a safe place,
or keep a copy of the key in a password manager application.
This key must be kept secret at all times.
''')

    def load(self, filename: str = None, password: str = None) -> None:
        '''Read masterkey from encrypted file
        May raise OSError, ValueError
        '''

        if filename is None:
            filename = self.filename

        # check filesize of the key (can not be larger than ...)
        if os.path.getsize(filename) > 1024:
            raise ValueError(f'{filename}: file is too large to be a masterkey')

        check_file_permissions(filename)

        if password is None:
            password = enter_password(verify=False, extra_prompt='masterkey')
        assert password is not None

        # decrypt masterkey file to obtain masterkey
        try:
            data = self.cryptor.decrypt_data(filename, password)
        except subprocess.CalledProcessError as err:
#            error(f'{err.cmd[0]}: process exited with returncode {err.returncode}')
            raise ValueError(f'{filename}: failed to decrypt masterkey; wrong passphrase?') from err

        self.key = data.rstrip(os.linesep)
#       debug(f'key == {self.key}')

        if not self.key:
            raise ValueError('invalid masterkey content')

    def save(self, out_filename: str = None) -> None:
        '''Invoke gpg to encrypt masterkey into file
        May raise ValueError, OSError, subprocess.CalledProcessError
        '''

        if not self.key:
            raise ValueError('invalid masterkey content')

        if out_filename is None:
            out_filename = self.filename

        if os.path.exists(out_filename):
            raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), out_filename)

        password = enter_password(verify=True, extra_prompt='new masterkey')

        if self.dryrun:
            info(f'not writing {out_filename}')
        else:
            info(f'writing {out_filename}')

        # set read-only for user
        os.umask(0o277)

        data = self.key + os.linesep
        self.cryptor.encrypt_data(data, out_filename, password, Masterkey.CIPHER)

        if self.dryrun:
            return

        # check output file exists and size > 0
        if not os.path.exists(out_filename):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), out_filename)

        if os.path.getsize(out_filename) <= 0:
            raise ValueError(f'{out_filename}: file is empty')

        check_file_permissions(out_filename)

    def change_password(self) -> None:
        '''Change the passphrase on the masterkey file
        May raise OSError, ValueError
        '''

        temp_filename = self.filename + '.tmp'
        if os.path.exists(temp_filename):
            raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), temp_filename)

        if not self.key:
            self.load()

        # encrypt the key to a new temp file
        try:
            self.save(temp_filename)
        except (OSError, ValueError, subprocess.CalledProcessError) as err:
            # delete temp file if it exists
            debug(f'cleaning up {temp_filename}')
            try:
                os.chmod(temp_filename, 0o600)
                os.unlink(temp_filename)
            except OSError:
                # ignore; probably no such file
                pass

            # re-raise error
            raise err

        if self.dryrun:
            return

        debug(f'moving {temp_filename} to {self.filename}')
        try:
            os.chmod(self.filename, 0o600)
            os.rename(temp_filename, self.filename)
        except OSError as err:
            raise OSError(err.errno, f'failed to move {temp_filename}', self.filename) from err
        else:
            info(f'saving {self.filename}')

    def __str__(self) -> str:
        '''Returns masterkey as string'''

        if not self.key:
            self.load()
        return self.key



class MyDirEntry:
    '''pseudo-DirEntry object for file stat information'''

    def __init__(self, direntry: Optional[os.DirEntry], path: str = None) -> None:
        '''initialize instance
        A MyDirEntry object can be constructed from an os.DirEntry instance
        or from a path
        MyDirEntry.__init__() does not know and does not care whether path actually exists
        '''

        self.direntry = direntry
        if self.direntry is not None:
            # Note: mypy gets completely lost because direnty can be None
            self.name = direntry.name   # type: ignore
            self.path = direntry.path   # type: ignore
        else:
            assert path is not None and path
            self.path = path
            self.name = os.path.basename(path)
        # cached stat() results
        self._statbuf: Optional[os.stat_result] = None
        self._lstatbuf: Optional[os.stat_result] = None

    def stat(self, follow_symlinks: bool = True) -> os.stat_result:
        '''Returns (cached) stat information
        May raise OSError, FileNotFoundError, PermissionError, etc.
        '''

        if self.direntry is not None:
            # mypy complains about follow_symlinks, but it really is documented like this ...
            return self.direntry.stat(follow_symlinks=follow_symlinks)  # type: ignore

        if follow_symlinks:
            if self._statbuf is None:
                self._statbuf = os.stat(self.path, follow_symlinks=True)
            return self._statbuf
        #else:
        if self._lstatbuf is None:
            self._lstatbuf = os.stat(self.path, follow_symlinks=False)
        return self._lstatbuf

    def is_dir(self, follow_symlinks=True) -> bool:
        '''Returns True if it's a directory
        May raise OSError, PermissionError, etc,
        but returns False if it does not exist
        '''

        if self.direntry is not None:
            return self.direntry.is_dir(follow_symlinks=follow_symlinks)

        try:
            statbuf = self.stat(follow_symlinks)
            return stat.S_ISDIR(statbuf.st_mode)
        except FileNotFoundError:
            return False

    def is_file(self, follow_symlinks=True) -> bool:
        '''Returns True if it's a regular file
        May raise OSError, PermissionError, etc,
        but returns False if it does not exist
        '''

        if self.direntry is not None:
            return self.direntry.is_file(follow_symlinks=follow_symlinks)

        try:
            statbuf = self.stat(follow_symlinks)
            return stat.S_ISREG(statbuf.st_mode)
        except FileNotFoundError:
            return False

    def is_symlink(self) -> bool:
        '''Returns True if it's a symbolic link
        May raise OSError, PermissionError, etc,
        but returns False if it does not exist
        '''

        if self.direntry is not None:
            return self.direntry.is_symlink()

        try:
            statbuf = self.stat(follow_symlinks=False)
            return stat.S_ISLNK(statbuf.st_mode)
        except FileNotFoundError:
            return False

    def inode(self) -> int:
        '''Returns inode number
        May raise OSError, FileNotFoundError, PermissionError, etc,
        '''

        if self.direntry is not None:
            return self.direntry.inode()

        statbuf = self.stat(follow_symlinks=False)
        return statbuf.st_ino



def is_excluded(path: str, exclude_patterns: List[str]) -> bool:
    '''Returns True if path matches any exclude pattern'''

    basename = os.path.basename(path)
    for pattern in exclude_patterns:
        if fnmatch.fnmatch(basename, pattern) or fnmatch.fnmatch(path, pattern):
            return True

    return False


def rscandir(path: str, exclude_patterns: List[str], error_list: List[Tuple[str, str]],
             continue_on_error: bool = True) -> Generator[MyDirEntry, None, None]:
    '''recursive scandir
    path must be a directory
    Yields only regular files, not directories, not symlinks or other
    Reports warnings for non-regular files
    Puts error information into error_list: [(filename, errmsg),]
    May raise OSError, NotADirectoryError
    '''

    # symbolic links are acceptable at the toplevel
    # but we don't follow symlinks down the directory tree

    # if continue_on_error is True then errors get caught down the tree
    # but mind that it may still raise an exception at the top level

    if is_excluded(path, exclude_patterns):
        debug(f'{path}: is excluded')
        return

    # scan the directory with os.scandir()
    # which will raise OSError if it is not a directory

    for entry in os.scandir(path):
        if is_excluded(entry.path, exclude_patterns):
            debug(f'{entry.path}: is excluded')
            continue

        try:
            if entry.is_dir(follow_symlinks=False):
                # recurse into directory
                yield from rscandir(entry.path, exclude_patterns, error_list, continue_on_error)

            elif entry.is_file(follow_symlinks=False):
                yield MyDirEntry(entry)

            else:
                error_list.append((entry.path, 'not a regular file'))
                warning(f'{entry.path}: not a regular file')

        except OSError as err:
            error_list.append((err.filename, err.strerror))
            if continue_on_error:
                error(f'{err.filename}: {err.strerror}')
            else:
                raise err


class BatchPath:
    '''path of filename to process in a Batch

    A BatchPath remembers its toplevel source dir,
    and can be used to construct a destination path
    '''

    __slots__ = 'srcpath', 'relpath'    # use less memory

    def __init__(self, srcpath: str, relpath: str) -> None:
        '''srcpath is toplevel directory that was scanned
        relpath is the relative path of the filename under srcpath
        '''

        assert relpath[0] != os.sep

        self.srcpath = srcpath
        self.relpath = relpath

    def __str__(self) -> str:
        '''Returns fullpath'''

        return os.path.join(self.srcpath, self.relpath)

    def destpath(self, destdir: str) -> str:
        '''Returns destpath for this path under destdir'''

        return os.path.join(destdir, self.relpath)



class BatchError(Exception):
    '''raised on error while handling a Batch
    A Batch is a collection of files; BatchError is a collection of files that
    errored. Usually a Batch is fully processed, errors are collected and
    a BatchError is raised at the very end, unless continue_on_error is False.
    '''

    def __init__(self) -> None:
        '''initialize instance'''

        super().__init__()
        self.errors: List[Tuple[str, str]] = []

    def __len__(self) -> int:
        '''Return number of errors'''

        return len(self.errors)

    def add(self, filename: str, errmsg: str) -> None:
        '''Add a filename + error message to the list'''

        self.errors.append((filename, errmsg))

    def extend(self, err_list: List[Tuple[str, str]]):
        '''Extend the internal error list with err_list'''

        self.errors.extend(err_list)

    def __iadd__(self, other: 'BatchError') -> 'BatchError':
        '''Add a BatchError'''

        self.errors.extend(other.errors)
        return self

    def __add__(self, other: 'BatchError') -> 'BatchError':
        '''Add up two BatchErrors'''

        berr = BatchError()
        berr.errors = self.errors[:]
        berr.errors.extend(other.errors)
        return berr

    def items(self) -> List[Tuple[str, str]]:
        '''Access to the list of errors'''

        return self.errors



class BatchFullError(Exception):
    '''raised when a batch becomes full'''



class Batch:
    '''represents a (sub)list of files to process
    In general you call Batch.make() to create Batches
    '''

    # DMF client utils
    CMD_DMVERSION = '/usr/bin/dmversion'
    CMD_DMGET = '/usr/bin/dmget'
    CMD_DMPUT = '/usr/bin/dmput'
    CMD_PIPE_DMGET = CMD_DMGET + ' -0 -q'
    CMD_PIPE_DMPUT = CMD_DMPUT + ' -0 -n'

    # limits on file count + batch size
    MAXCOUNT = 1000
    MAXSIZE = 1024 ** 4
    HAVE_DMF_INIT: bool = False
    HAVE_DMF: bool = False

    def __init__(self, filelist: List[BatchPath] = None, continue_on_error: bool = True, dryrun: bool = False) -> None:
        '''initialize instance'''

        if filelist is None:
            self.filelist: List[BatchPath] = []
        else:
            self.filelist = filelist
        self.continue_on_error = continue_on_error
        self.dryrun = dryrun
        # warning: if a filelist was passed, then size will be off
        # We could repair the size but note that it would essentially be
        # unnecessary dead code at this moment
        self.size = 0
        if not Batch.HAVE_DMF_INIT:
            Batch._detect_dmf()

    @staticmethod
    def _detect_dmf() -> None:
        '''See if the DMF client utils are present
        Initializes Batch.HAVE_DMF ; the outcome is cached
        '''

        if Batch.HAVE_DMF_INIT:
            # already initialized
            return

        have_dmf = is_exec(Batch.CMD_DMVERSION) and is_exec(Batch.CMD_DMGET) and is_exec(Batch.CMD_DMPUT)
        if have_dmf:
            status = 'installed'
        else:
            status = 'not installed'
        debug(f'have_dmf == {have_dmf}; dmf client utils are {status}')
        Batch.HAVE_DMF = have_dmf
        Batch.HAVE_DMF_INIT = True

    @staticmethod
    def make(srclist: List[str], exclude_patterns: List[str], errors: BatchError,
             continue_on_error: bool, dryrun: bool = False) -> Generator['Batch', None, None]:
        '''generator that yields batches of files
        for a given list of sources: files and/or directories

        Afterwards 'errors' contains list of files that had errors
        during traversing the directory structure (e.g. for PermissionError)

        May raise OSError if continue_on_error is False
        '''

        batch = Batch(None, continue_on_error, dryrun)
        for src in srclist:
            # strip double slashes, trailing slashes
            src = os.path.normpath(src)

            debug(f'scanning {src}')

            if is_excluded(src, exclude_patterns):
                debug(f'{src}: is excluded')
                continue

            # determine the source path for entries given on the command-line
            # The source path is set per batch
            # Knowing the source path helps to relocate files with --destdir
            # For directories, the source path is the given dir itself
            # For files, the source path is '/' for abspath and '.' for relative path
            # It may be that the given name does not exist at all;
            # rscandir() will report an error for that, so we don't check here

            entry = MyDirEntry(None, src)
            if entry.is_file():
                # src is a file entry
                if src[0] == os.sep:
                    srcpath = os.sep
                else:
                    srcpath = ''

                try:
                    batch.add(srcpath, entry.path, entry.stat().st_size)
                except BatchFullError:
                    yield batch

                    # retry; add it again
                    # this should never raise a BatchFullError exception
                    # but mind that entry.stat() might raise OSError
                    batch = Batch(None, continue_on_error, dryrun)
                    batch.add(srcpath, entry.path, entry.stat().st_size)

                except OSError as err:
                    errors.add(err.filename, err.strerror)
                    if continue_on_error:
                        error(f'{err.filename}: {err.strerror}')
                    else:
                        raise err
            else:
                # src is a directory (or not does exist at all)
                srcpath = src
                try:
                    error_list: List[Tuple[str, str]] = []
                    for entry in rscandir(src, exclude_patterns, error_list, continue_on_error):
                        # move any errors to our BatchError instance
                        # (rscandir() does not depend on BatchError)
                        errors.extend(error_list)
                        error_list = []

                        try:
                            batch.add(srcpath, entry.path, entry.stat().st_size)
                        except BatchFullError:
                            yield batch

                            # retry; add it again
                            # this should never raise a BatchFullError exception
                            # but mind that entry.stat() might raise OSError
                            batch = Batch(None, continue_on_error, dryrun)
                            batch.add(src, entry.path, entry.stat().st_size)

                        except OSError as err:
                            # exception in Batch.add()
                            errors.add(err.filename, err.strerror)
                            if continue_on_error:
                                error(f'{err.filename}: {err.strerror}')
                            else:
                                raise err

                except OSError as err:
                    # exception in rscandir(src)
                    errors.add(err.filename, err.strerror)
                    if continue_on_error:
                        error(f'{err.filename}: {err.strerror}')
                    else:
                        raise err

        if batch.filelist:
            yield batch

        debug('ending Batch.make()')

    def __len__(self) -> int:
        '''Returns number of files in the batch'''

        return len(self.filelist)

    def add(self, srcpath: str, path: str, size: int) -> None:
        '''add file with size to the batch

        srcpath may be empty for relative paths
        Note that srcpath and path are just strings; no effort is made
        to check that file/dir exists and is indeed a regular file

        Raises BatchFullError if the batch goes over limits
        '''

        # the same file can be added multiple times;
        # we don't bother checking (because no UNIX cmd does)

        # if the batch goes over limits, raise BatchFullError
        # A single file is always allowed no matter what its size is
        if len(self.filelist) + 1 > Batch.MAXCOUNT:
            raise BatchFullError

        if len(self.filelist) > 1 and self.size + size > Batch.MAXSIZE:
            raise BatchFullError

        # append trailing slash if it is not there
        if srcpath and srcpath[-1] != os.sep:
            srcpath += os.sep
            assert path[:len(srcpath)] == srcpath

        self.filelist.append(BatchPath(srcpath, path[len(srcpath):]))
        self.size += size

    def remove(self, filename: str) -> None:
        '''remove entry from filelist'''

        # not super efficient ... but no other good way of doing it
        # No ValueError is raised if filename is not in list (don't care)
        self.filelist = [x for x in self.filelist if str(x) != filename]

    def dmget(self) -> None:
        '''dmget batch of files
        Note that this function may print error messages,
        but it effectively ignores any errors
        '''

        if not Batch.HAVE_DMF or not self.filelist:
            return

        info(f'{PROG} is dmgetting any offline files')
        try:
            self._dmputget(Batch.CMD_PIPE_DMGET)

        except OSError as err:
            error(f'{err.filename}: {err.strerror}')

        except subprocess.CalledProcessError as err:
            # return code 1 also happens for non-DMF filesystem; ignore
            if err.returncode != 1:
                warning(f'{err.cmd[0]}: process exited with returncode {err.returncode}')

    def dmput(self) -> None:
        '''dmput list of files
        Note that this function may print error messages,
        but it effectively ignores any errors
        '''

        try:
            self._dmputget(Batch.CMD_PIPE_DMPUT)

        except OSError as err:
            error(f'{err.filename}: {err.strerror}')

        except subprocess.CalledProcessError as err:
            # return code 1 also happens for non-DMF filesystem; ignore
            if err.returncode != 1:
                warning(f'{err.cmd[0]}: process exited with returncode {err.returncode}')

    def _dmputget(self, dm_cmd: str) -> None:
        '''dmput or dmget filelist
        May raise OSError, subprocess.CalledProcessError
        '''

        if not Batch.HAVE_DMF or not self.filelist:
            return

        # make nul-terminated list of filenames
        data = '\0'.join([str(x) for x in self.filelist])

        cmd_arr = split_cmd(dm_cmd)
        debug(f'exec: |{dm_cmd}')
        if not self.dryrun:
            subprocess.run(cmd_arr, input=bytes(data.encode()), shell=False, check=True)



class AppStats:
    '''Application statistics'''

    def __init__(self) -> None:
        '''initialize instance'''

        self.filecount = 0
        self.errcount = 0
        self.size = 0
        self.duration = 0.0
        self._timer = 0.0

    def __iadd__(self, other: 'AppStats') -> 'AppStats':
        '''add up statistics'''

        self.filecount += other.filecount
        self.errcount += other.errcount
        self.size += other.size
        self.duration += other.duration
        return self

    def start_timer(self) -> None:
        '''start the internal duration timer'''

        self._timer = time.monotonic()

    def end_timer(self) -> float:
        '''Returns elapsed time since start_timer()
        as a float in seconds
        '''

        return time.monotonic() - self._timer

    def report(self, decrypt: bool) -> None:
        '''report statistics'''

        print(f'{self.filecount} files, {self.errcount} errors')

        if (self.filecount > 0 and self.errcount < self.filecount) or self.duration > 10.0:
            try:
                bps = int(self.size / self.duration)
            except ZeroDivisionError:
                bps = self.size

            if decrypt:
                s_crypting = 'decrypting'
            else:
                s_crypting = 'encrypting'
            s_duration = AppStats.sprint_duration(self.duration)
            print(f'{s_crypting} took {s_duration}')
            s_size = Metric(self.size).format()
            debug(f'size == {self.size} bytes')
            debug(f'duration == {self.duration} seconds')
            s_bps = Metric(bps).format()
            print(f'total {s_size} at {s_bps}/s')

    @staticmethod
    def sprint_duration(delta: float) -> str:
        '''Returns time duration in seconds as human-readable string'''

        # delta is a float, but we don't deal with fractions of a second
        # so use int(delta) and make everything integers
        days, remainder = divmod(int(delta), 3600 * 24)
        hours, remainder = divmod(remainder, 3600)
        mins, secs = divmod(remainder, 60)

        parts: List[str] = []
        if days:
            s_days = 'days' if days > 1 else 'day'
            parts.append(f'{days} {s_days}')

        if hours:
            s_hours = 'hours' if hours > 1 else 'hour'
            parts.append(f'{hours} {s_hours}')

        if mins:
            s_mins = 'minutes' if mins > 1 else 'minute'
            parts.append(f'{mins} {s_mins}')

        if secs >= 10:
            parts.append(f'{secs} seconds')

        if not parts:
            return 'less than 10 seconds'

        if len(parts) > 1:
            s_duration = ', '.join(parts[:-1])
            s_duration += ' and ' + parts[-1]
            return s_duration

        return parts[0]



class BatchCrypt(Batch):
    '''Encrypt/decrypt a batch of files'''

    # This class groups functions that do encryption of Batches
    # It is not a generic class as it is very specific to this application

    def __init__(self, filelist: List[BatchPath] = None, size: int = 0,
                 continue_on_error: bool = True, dryrun: bool = False) -> None:
        '''initialize instance'''

        super().__init__(filelist, continue_on_error, dryrun)
        self.size = size
        self.stats = AppStats()

    def crypt(self, cryptor: Cryptor, password: str, cipher: str = None, destdir: str = None,
              decrypt: bool = False, display_progressbar: bool = False) -> None:
        '''encrypt/decrypt a batch with password
        cipher may be None, in which case the default cipher will be used
        In case of decryption, cipher is not used
        Also stages input/output files and verifies that the output file exists afterwards

        May raise BatchError, ValueError (eg. invalid cipher)
        or OSError, subprocess.CalledProcessError if continue_on_error is False
        '''

        # remove any files that already are encrypted/not encrypted
        self.remove_already_crypted(decrypt)

        # stage input files
        self.dmget()

        # this will be a Batch of output files
        output_batch = Batch(None, self.continue_on_error, self.dryrun)

        errors = BatchError()

        for batchpath in self.filelist:
            try:
                output = BatchCrypt.make_output_batchpath(batchpath, destdir, decrypt)
            except ValueError as err:
                # file is already crypted
                # This shouldn't happen because we already filtered the batch
                warning(f'{err}')
                continue

            input_filename = str(batchpath)
            output_filename = str(output)
            try:
                # run the cryptor
                # keep cumulative time of how long it takes
                self.stats.start_timer()
                if decrypt:
                    cryptor.decrypt_file(input_filename, output_filename, password, display_progressbar)
                else:
                    cryptor.encrypt_file(input_filename, output_filename, password, cipher, display_progressbar)
                self.stats.duration += self.stats.end_timer()

                if not self.dryrun:
                    # see if the output file exists
                    if not os.path.exists(output_filename):
                        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), output_filename)
                    if not os.path.isfile(output_filename):
                        # use errno 0 ... I really like this condition to be an OSError
                        raise OSError(0, 'not a regular file', output_filename)

                    # Note, we do not check if the output filesize > 0
                    # (which may be legitimate, but is suspicious at the least)
                    # we rely on GPG exit status to detect any write failures

            except FileNotFoundError as err:
                warning(f'{err.filename}: file disappeared while we were busy')
                if self.stats.filecount > 0:
                    self.stats.filecount -= 1

            # note that FileExistsError is also an OSError
            except OSError as err:
                self.stats.errcount += 1
                if not self.continue_on_error:
                    # stop on errors; early exit
                    raise err

                error(f'{err.filename}: {err.strerror}')
                errors.add(input_filename, err.strerror)

            except subprocess.CalledProcessError as err:
                self.stats.errcount += 1
                if not self.continue_on_error:
                    # stop on errors; early exit
                    raise err

                error(f'{err.cmd[0]} exit status {err.returncode}')
                errors.add(input_filename, f'{err.cmd[0]} exit status {err.returncode}')

            else:   # no exception occurred
                self.stats.filecount += 1
                try:
                    self.stats.size += os.path.getsize(input_filename)
                except OSError:
                    warning(f'failed to get size of {input_filename}')

                output_batch.filelist.append(output)

        # save output files
        output_batch.dmput()

        # if any errors, raise BatchError exception
        # that holds the list of errored filenames
        if errors:
            raise errors

    def remove_already_crypted(self, decrypt: bool, suffix: str = Cryptor.SUFFIX) -> None:
        '''Removes any files from batch that already are encrypted/not encrypted'''

        new_filelist: List[BatchPath] = []
        for batchpath in self.filelist:
            filename = str(batchpath)
            if BatchCrypt.already_crypted(filename, decrypt, suffix):
                if decrypt:
                    info(f'{filename}: file is not encrypted')
                else:
                    info(f'{filename}: file already is encrypted')
                continue

            new_filelist.append(batchpath)
        self.filelist = new_filelist

    @staticmethod
    def already_crypted(filename: str, decrypt: bool, suffix: str = Cryptor.SUFFIX) -> bool:
        '''Returns True if filename is already crypted
        It sees this by looking at the filename suffix
        '''

        if suffix[0] != '.':
            suffix = '.' + suffix

        if decrypt:
            return not filename.endswith(suffix)
        return filename.endswith(suffix)

    @staticmethod
    def make_output_batchpath(batchpath: BatchPath, destdir: Optional[str], decrypt: bool = False, suffix: str = Cryptor.SUFFIX) -> BatchPath:
        '''Returns BatchPath with output filename or None if not crypted
        Output filenames have suffix when encrypted,
        and have no suffix when decrypted
        destdir may be None or empty; output will be placed next to source
        Raises ValueError if suffix does not correctly match
        '''

        if not destdir:
            destdir = batchpath.srcpath

        if suffix[0] != '.':
            suffix = '.' + suffix

        if decrypt:
            if not batchpath.relpath.endswith(suffix):
                # This should never happen because we already filtered the batch
                raise ValueError(f'{str(batchpath)}: file is not encrypted')

            out_filename, _ = os.path.splitext(batchpath.relpath)
        else:
            # encrypt
            if batchpath.relpath.endswith(suffix):
                # This should never happen because we already filtered the batch
                raise ValueError(f'{str(batchpath)}: file already is encrypted')

            out_filename = batchpath.relpath + suffix

        return BatchPath(destdir, out_filename)



class BatchDelete(Batch):
    '''delete a batch of files
    Also removes empty subdirectories
    '''

    # Note: this class extends class Batch and therefore
    # it could be incorporated into class Batch
    # But I reckon that I will keep it a separate functionality

#    def __init__(self)  same as for class Batch

    def delete_file(self, filename: str) -> None:
        '''delete file
        May raise OSError (eg, PermissionError)
        but does not give an error if the file does not exist
        '''

        try:
            os.unlink(filename)
        except OSError as err:
            if err.errno == errno.ENOENT:
                # no such file; already removed by someone
                info(f'deleting {filename}')
            else:
                raise err
        else:
            info(f'deleting {filename}')

    @staticmethod
    def remove_dir(dirname: str) -> None:
        '''remove empty subdirectory
        May raise OSError (eg, PermissionError)
        but does not give an error if the dir does not exist or is not empty
        '''

        try:
            os.rmdir(dirname)
        except OSError as err:
            if err.errno == errno.ENOENT:
                # no such dir; already removed
                info(f'removing {dirname}')
            elif err.errno == errno.ENOTEMPTY:
                # directory not empty
                pass
            else:
                raise err
        else:
            info(f'removing {dirname}')

    def delete(self, remove_empty_dirs: bool = True) -> None:
        '''delete files
        Raises BatchError for any errors that occur
        or OSError if continue_on_error is False
        '''

        errors = BatchError()

        for batchpath in self.filelist:
            # delete file
            filename = str(batchpath)
            if self.dryrun:
                info(f'not deleting {filename}')
                continue

            try:
                self.delete_file(filename)
            except OSError as err:
                if not self.continue_on_error:
                    raise err
                errors.add(filename, f'delete failed: {err.strerror}')
            else:
                if not remove_empty_dirs:
                    continue

                # try remove directory
                subdir = os.path.dirname(batchpath.relpath)
                if not subdir:
                    # there is no subdir
                    continue

                dirname = os.path.join(batchpath.srcpath, subdir)
                try:
                    BatchDelete.remove_dir(dirname)
                except OSError as err:
                    if not self.continue_on_error:
                        raise err
                    errors.add(dirname, f'delete failed: {err.strerror}')

        if errors:
            raise errors



def check_srcpath(path: str) -> str:
    '''Checks that path is a valid path
    Returns path with trailing slashes stripped (if any)
    May raise ValueError, OSError, FileNotFoundError, PermissionError, etc.
    '''

    if not path:
        raise ValueError("invalid path: ''")

    # strip double slashes, trailing slashes
    path = os.path.normpath(path)

    # this might produce FileNotFoundError, PermissionError, OSError
    _ = os.stat(path)

    # Note that this may produce OSError (which is fine)
    if os.path.isdir(path):
        pass

    elif not os.path.isfile(path):
        raise ValueError(f'{path}: not a regular file')

    return path


def load_exclude(filename: str) -> List[str]:
    '''load exclude patterns from file
    Returns list of patterns
    May raise OSError
    '''

    f = open(filename)
    with f:
        lines = f.readlines()

    excluded = [x.strip() for x in lines if x]
    return excluded


def crypt_files(opts: argparse.Namespace, sources: List[str], stats: AppStats = None) -> None:
    '''encrypt or decrypt given source list of files/directories
    This is the meat of the main application for encryption/decryption

    May raise ValueError, OSError if source list is not OK
    May raise BatchError at the end to indicate that there were errors
    '''

    # give warning about open umask
    check_umask()

    # check the source filelist
    # the sources given on the command-line must be OK otherwise
    # we may bail out here with an exception
    srclist = [check_srcpath(x) for x in sources]

    # make exclusion list
    if opts.exclude is not None:
        excluded = opts.exclude[:]
    else:
        excluded = []
    if opts.exclude_from is not None:
        excluded.extend(load_exclude(opts.exclude_from))

    # make cryptor object; encryption engine
    cryptor = Cryptor(opts.cmd_gpg, dryrun=opts.dryrun)

    # get passphrase
    if opts.masterkey is not None:
        key = Masterkey(opts.masterkey, cryptor, opts.dryrun)
        if opts.batch is not None:
            batch_passphrase = load_password(opts.batch)
            key.load(password=batch_passphrase)
        else:
            key.load()
        # encryption key is the masterkey
        passphrase = key.key
    else:
        # or load passphrase from file
        if opts.batch is not None:
            passphrase = load_password(opts.batch)
        else:
            # else ask for passphrase
            passphrase = enter_password(verify=(opts.decrypt is False))
    assert passphrase is not None

    errors = BatchError()

    for batch in Batch.make(srclist, excluded, errors, not opts.bail, opts.dryrun):
        # convert the bare Batch into a BatchCrypt instance
        batch = BatchCrypt(batch.filelist, batch.size, batch.continue_on_error, batch.dryrun)

        # encrypt/decrypt batch
        error_filenames_in_this_batch: List[str] = []
        try:
            batch.crypt(cryptor, passphrase, opts.cipher, opts.destdir, opts.decrypt, opts.progress)
        except BatchError as err:
            error_filenames_in_this_batch = [x[0] for x in err.items()]
            # pack all errors together
            errors += err

        # add up all the stats
        # Note that delete errors are not counted, and
        # delete time is not counted
        if stats is not None:
            stats += batch.stats

        if opts.delete:
            # delete files that were successfully crypted
            # which is the input batch but without files that errored
            delete_filelist = [x for x in batch.filelist if str(x) not in error_filenames_in_this_batch]
            delete_batch = BatchDelete(delete_filelist, dryrun=opts.dryrun)
            try:
                delete_batch.delete()
            except BatchError as err:
                # report errors now
                for filename, errmsg in err.items():
                    error(f'{filename}: {errmsg}')
                # propagate these errors
                errors += err

    if errors:
        # raise BatchError containing list of errored files
        raise errors


def generate_key(opts: argparse.Namespace, filename: str) -> None:
    '''Generate new masterkey file
    May raise FileExistsError, subprocess.CalledProcessError
    '''

    # Note: there is no check_umask() here
    # because Masterkey.save() will force umask 0277 (read-only user)

    if opts.cipher != Masterkey.CIPHER:
        info(f'Note: the masterkey is always encrypted with {Masterkey.CIPHER}')

    cryptor = Cryptor(opts.cmd_gpg, opts.dryrun)
    key = Masterkey(filename, cryptor, opts.dryrun)
    key.generate()


def change_keypass(opts: argparse.Namespace, filename: str) -> None:
    '''Change the passphrase on the masterkey file
    May raise OSError, ValueError
    '''

    if opts.cipher != Masterkey.CIPHER:
        info(f'Note: the masterkey is always encrypted with {Masterkey.CIPHER}')

    cryptor = Cryptor(opts.cmd_gpg, opts.dryrun)
    key = Masterkey(filename, cryptor, opts.dryrun)
    key.change_password()


def check_umask() -> None:
    '''produce warning if umask is too open'''

    # only way to get current umask, is to call it twice
    orig_umask = os.umask(0o27)
    os.umask(orig_umask)

    perms = 0o666 & ~orig_umask
    if perms & 0o066 != 0:
        if perms & 0o060 != 0:
            msg = 'group'
            if perms & 0o006 != 0:
                msg += ' and others'
        elif perms & 0o006 != 0:
            msg = 'others'

        warning(f'umask is {orig_umask:04o}; newly created files will be readable by {msg}')


def check_file(filename: str) -> None:
    '''Check whether file exists
    Raises ValueError if no such file
    '''

    if not os.path.exists(filename):
        raise ValueError(f'{filename}: no such file')

    if not os.path.isfile(filename):
        raise ValueError(f'{filename}: not a regular file')


def check_dir(dirname: str) -> None:
    '''Check whether dir exists
    Produces a warning if other users have access
    Raises ValueError if no such dir
    '''

    if not os.path.exists(dirname):
        raise ValueError(f'{dirname}: no such directory')

    if not os.path.isdir(dirname):
        raise ValueError(f'{dirname}: not a directory')

    # give a warning if the directory has open permissions
    if os.path.isabs(dirname):
        fullpath = dirname
    else:
        # if relative path, put cwd in front
        fullpath = os.path.join(os.getcwd(), dirname)
    if is_opendir(fullpath):
        warning(f'{dirname}: directory is accessible by group and/or others')


def check_masterkey_file(filename: str) -> None:
    '''Quick check whether masterkey file exists
    Raises ValueError if no such file
    '''

    # masterkey files must end with .gpg
    if not filename.endswith(Cryptor.SUFFIX):
        filename += Cryptor.SUFFIX

    if not os.path.isfile(filename):
        raise ValueError(f'{filename}: no such masterkey file')


def check_missing_masterkey_file(filename: str) -> None:
    '''Quick check that masterkey file does not already exist
    Raises ValueError if it does
    '''

    # masterkey files must end with .gpg
    if not filename.endswith(Cryptor.SUFFIX):
        filename += Cryptor.SUFFIX

    if os.path.exists(filename):
        raise ValueError(f'{filename}: masterkey file already exists')


def python_version_check(major: int, minor: int, micro: int) -> None:
    '''check that python version is older than major,minor,micro'''

    if major != sys.version_info[0]:
        raise RuntimeError('error: python version %d.%d.%d is required' %
                           (major, minor, micro))

    version = minor << 16
    version |= micro
    python_version = sys.version_info[1] << 16
    python_version |= sys.version_info[2]
    if python_version < version:
        raise RuntimeError('error: python version %d.%d.%d or '
                           'later is required' % (major, minor, micro))


class ArgsOption:
    '''represents a single option for the ArgsParser'''

    def __init__(self, *args, metavar='', help_msg=''):
        '''initialize instance'''

        self.options = args
        self.metavar = metavar
        self.help_msg = help_msg

    def __str__(self):
        '''Returns a single line help message'''

        if len(self.options) > 1:
            s_option = ', '.join(self.options)
        else:
            # determine whether self.options[0] is a short or a long option
            if len(self.options[0]) <= 2:
                s_option = self.options[0]
            else:
                # right justified
                s_option = '    ' + self.options[0]

        if self.metavar:
            s_option += '=' + self.metavar

        return f'  {s_option:<24} {self.help_msg}'



class ArgsParser(argparse.ArgumentParser):
    '''custom argument parser based off argparse.ArgumentParser
    which prints a nice(r) help text
    '''

    def __init__(self, **kwargs):
        '''initialize instance'''

        super().__init__(**kwargs)

        self.options = []
        try:
            self.epilog = kwargs['epilog']
        except KeyError:
            self.epilog = ''

    def add_argument(self, *args, **kwargs):
        '''add an option or argument'''

        if not args or (len(args) == 1 and args[0][0] != '-'):
            # not an option
            pass
        else:
            # one or more options in *args
            try:
                help_msg = kwargs['help']
            except KeyError:
                help_msg = ''
            try:
                metavar = kwargs['metavar']
            except KeyError:
                metavar = ''
            option = ArgsOption(*args, metavar=metavar, help_msg=help_msg)
            self.options.append(option)
        # pass it on
        super().add_argument(*args, **kwargs)

    def add_title(self, title):
        '''add a special line to the help text'''

        if title:
            title = '\n' + title

        # just add the string to the options list
        # it will be printed by print_help()
        self.options.append(title)

    def print_help(self, _file=None):
        '''print custom help message

        Note: printing to file is not supported
        '''

        self.print_usage()
        print()

        for option in self.options:
            print(str(option))

        if self.epilog:
            print()
            print(self.epilog)



def get_options() -> argparse.Namespace:
    '''parse command-line options
    Returns argparse options object
    Exits on error
    '''

    parser = ArgsParser(prog=PROG, usage=f'{PROG} [-m KEY] [-d] [options] FILE|DIR [..]', add_help=False)
    # custom help message, see below
    parser.add_argument('-h', '--help', action='store_true', help='show this help message and exit')
    parser.add_argument('-d', '--decrypt', action='store_true', help='decrypt encrypted files')
    parser.add_argument('-p', '--progress', action='store_true', help='show progress bar')
    parser.add_argument('-m', '--master', dest='masterkey', metavar='KEYFILE', help='use masterkey file for encrypt/decrypt')
    parser.add_argument('-g', '--genkey', metavar='KEYFILE', help='generate new masterkey')
    parser.add_argument('--change', dest='changekey', metavar='KEYFILE', help='change passphrase on masterkey file')
    parser.add_argument('-x', '--exclude', action='append', metavar='PATTERN', help='exclude files matching PATTERN')
    parser.add_argument('-X', '--exclude-from', metavar='FILE', help='read exclude patterns from FILE')
    parser.add_argument('--delete', action='store_true', help='delete source file after crypting')
    parser.add_argument('-D', '--destdir', metavar='DESTDIR', help='place output files under DESTDIR')
    parser.add_argument('-b', '--batch', metavar='FILE', help='read passphrase from FILE')
    parser.add_argument('--bail', action='store_true', help='stop on error (early exit)')
    parser.add_argument('--werror', action='store_true', help='all warnings and errors are fatal errors')
    parser.add_argument('--stats', action='store_true', help='report statistics after crypting')
    parser.add_argument('--cipher', metavar='CIPHER', default=Cryptor.DEFAULT_CIPHER,
                        help=f"set encryption cipher  (default: {Cryptor.DEFAULT_CIPHER})")
    parser.add_argument('--gpg', dest='cmd_gpg', default=Cryptor.DEFAULT_GPG, metavar='PATH',
                        help=f"use alternative GPG executable (default: {Cryptor.DEFAULT_GPG})")
    parser.add_argument('--debug', action='store_true', help='show debug output')
    parser.add_argument('-n', '--dry-run', action='store_true', dest='dryrun', help='do not actually crypt or delete files')
    parser.add_argument('-q', '--quiet', action='store_true', help='suppress informational messages and warnings')
    # Note: unusual version action ... because
    # argparse won't print my copyright line nicely
    parser.add_argument('--version', action='store_true', help='show version info and exit')
    parser.add_argument('args', nargs=argparse.REMAINDER, metavar='FILE|DIR [..]')
    opts = parser.parse_args()

    if opts.debug:
        # enable the debug function by redefining it
        thismodule = sys.modules[__name__]
        setattr(thismodule, 'debug', debug_msg)

    debug(f'opts.help: {opts.help}')
    debug(f'opts.decrypt: {opts.decrypt}')
    debug(f'opts.progress: {opts.progress}')
    debug(f'opts.masterkey: {opts.masterkey}')
    debug(f'opts.genkey: {opts.genkey}')
    debug(f'opts.changekey: {opts.changekey}')
    debug(f'opts.exclude: {opts.exclude}')
    debug(f'opts.exclude_from: {opts.exclude_from}')
    debug(f'opts.delete: {opts.delete}')
    debug(f'opts.destdir: {opts.destdir}')
    debug(f'opts.batch: {opts.batch}')
    debug(f'opts.bail: {opts.bail}')
    debug(f'opts.werror: {opts.werror}')
    debug(f'opts.stats: {opts.stats}')
    debug(f'opts.cipher: {opts.cipher}')
    debug(f'opts.cmd_gpg: {opts.cmd_gpg}')
    debug(f'opts.debug: {opts.debug}')
    debug(f'opts.dryrun: {opts.dryrun}')
    debug(f'opts.quiet: {opts.quiet}')
    debug(f'opts.version: {opts.version}')
    debug(f'opts.args: {opts.args}')

    # check options
    try:
        if opts.help:
            # we have a custom help function; displays supported ciphers
            # we get the list of supported ciphers later, and
            # turn exceptions into warnings because
            # we still want to see the help text in case anything goes wrong

            parser.print_help()

            all_ciphers: List[str] = []
            try:
                cryptor = Cryptor(cmd_gpg=opts.cmd_gpg)
                all_ciphers = cryptor.all_ciphers
            except OSError as err:
                warning(f'{err.strerror}')
            except RuntimeError:
                # only raised for empty cipher list, ignore for now
                pass
            except ValueError as err:
                warning(f'{err}')

            if not all_ciphers:
                epilog = '\nwarning: empty list of ciphers, gpg command not behaving as expected'
            else:
                ciphers = ', '.join(all_ciphers)
                epilog = f'\nCiphers: {ciphers}'

            selected_cipher = opts.cipher.upper()
            if selected_cipher not in all_ciphers:
                epilog += f'\nwarning: cipher {selected_cipher} is not supported\n'

            print(epilog)
            sys.exit(0)

        if opts.version:
            print(f'''saracrypt {VERSION}
Copyright 2018 by SURFsara B.V.''')
            sys.exit(0)

        if opts.quiet:
            # suppress output
            thismodule = sys.modules[__name__]
            setattr(thismodule, 'info', suppress_output)
            setattr(thismodule, 'warning', suppress_output)

        if opts.werror:
            # warnings and errors are fatal errors
            opts.bail = True
            thismodule = sys.modules[__name__]
            setattr(thismodule, 'error', fatal_error)
            setattr(thismodule, 'warning', fatal_error)

        # check that any filenames passed on cmdline truly exist
        # this is a double check, but allows us to exit early
        # on such a trivial user error

        if opts.masterkey is not None:
            if opts.genkey is not None:
                parser.error('options --master and --genkey can not be combined')
            if opts.changekey is not None:
                parser.error('options --master and --changekey can not be combined')
            check_masterkey_file(opts.masterkey)

        if opts.genkey and opts.changekey:
            parser.error('options --genkey and --changekey can not be combined')

        if opts.genkey is not None:
            # check that file not already exists
            check_missing_masterkey_file(opts.genkey)

        if opts.changekey is not None:
            check_masterkey_file(opts.changekey)

        if opts.exclude_from is not None:
            check_file(opts.exclude_from)

        if opts.destdir is not None:
            if opts.genkey or opts.changekey:
                parser.error('option --destdir is only for data encryption/decryption')
            check_dir(opts.destdir)

        if opts.batch is not None:
            check_file(opts.batch)
            if opts.quiet:
                opts.progress = False
                setattr(thismodule, 'print_progress', suppress_output)

        # make uppercase
        opts.cipher = opts.cipher.upper()

        if not sys.stdout.isatty():
            # turn off progress bar
            opts.progress = False

    except ValueError as err:
        error(f'{err}')
        sys.exit(1)

    return opts


def scoped_main() -> None:
    '''main program'''

    global DEBUG_COLORS

    if not sys.stdout.isatty():
        # turn off debug color codes
        DEBUG_COLORS = False

    # disable core dumps
    # so that you can not read the password from a core dump
    resource.setrlimit(resource.RLIMIT_CORE, (0, 0))

    opts = get_options()

    # make sure that ~/.gnupg is proper
    Cryptor.check_gnupg_dir()

    if opts.dryrun:
        print('''
DRY RUN, actions not performed
''')

    if opts.genkey is not None:
        generate_key(opts, opts.genkey)

    elif opts.changekey is not None:
        change_keypass(opts, opts.changekey)

    else:
        # else action is encrypt or decrypt
        if not opts.args:
            # short usage
            print(f'usage: {PROG} [-m KEY] [-d] [options] FILE|DIR [..]')
            sys.exit(1)

        stats = AppStats() if opts.stats else None
        try:
            crypt_files(opts, opts.args, stats)
        except BatchError as err:
            # display list of errored files
            info('')    # put an empty line
            error('summary of encountered problems:')
            for filename, errmsg in err.items():
                error(f'{filename}: {errmsg}')
            sys.exit(-1)
        finally:
            if opts.stats:
                # report statistics
                stats.report(opts.decrypt)      # type: ignore  # mypy knows stats can be None



if __name__ == '__main__':
    try:
        python_version_check(3, 6, 3)
        scoped_main()

    except OSError as err:
        error(f'{err.filename}: {err.strerror}')
        sys.exit(-1)

    except subprocess.CalledProcessError as err:
        error(f'{err.cmd[0]} exited with returncode {err.returncode}')
        sys.exit(-1)

    except (ValueError, RuntimeError) as err:
        error(f'{err}')
        sys.exit(-1)

    except KeyboardInterrupt:
        print('\nInterrupted by Ctrl-C')
        sys.exit(130)

# EOB
