#! /bin/bash
#
#	build_rpm.sh
#

if [[ -z $1 ]]
then
	echo "usage: build_rpm.sh <release-tag>"
	exit 1
fi

if [[ ! -e saracrypt.py ]]
then
	echo "error: please chdir to the source directory"
	exit 1
fi

python3 --version

CI_COMMIT_TAG=$1

# this is necessary when building by hand on the command-line
git checkout tags/$CI_COMMIT_TAG

./ci_build.sh

TAG_VERSION=$(echo $CI_COMMIT_TAG | awk -F- '{ print $2 }')
TAG_RELEASE=$(echo $CI_COMMIT_TAG | awk -F- -v dist="$DIST" '{ if ($3) { print dist $3 } else { print dist 1 } }')
rpmbuild -ba \
	--define "tag_version $TAG_VERSION" \
	--define "tag_release $TAG_RELEASE" \
	--define "_topdir $(pwd)" \
	--define "_sourcedir $(pwd)" \
	saracrypt.spec

git checkout master

# EOB

