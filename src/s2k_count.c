/*
 *	s2k_count.c		WJ118
 *
 *	* get the s2k count by running a KDF for about 100 ms
 *
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <gcrypt.h>

/*
#define DEBUG 1
#define MAIN 1
*/

typedef unsigned long ulong;

static int did_init_libgcrypt = 0;

static void init_libgcrypt(void) {
	if (did_init_libgcrypt) {
		return;
	}

	/*
		version check should be the very first call because it
		makes sure that important subsystems are initialized
	*/
	if (!gcry_check_version(GCRYPT_VERSION)) {
		fprintf(stderr, "libgcrypt version mismatch\n");
		exit(2);
	}

	/* disable secure memory (not needed) */
	gcry_control(GCRYCTL_DISABLE_SECMEM, 0);

	/* ... if required, other initialization goes here */

	/* tell Libgcrypt that initialization has completed */
	gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);

	did_init_libgcrypt = 1;
}

static ulong timediff_msecs(const struct timespec *start, const struct timespec *end) {
	ulong m0 = start->tv_sec * 1000UL + start->tv_nsec / 1000000UL;
	ulong m1 = end->tv_sec * 1000UL + end->tv_nsec / 1000000UL;
	return m1 - m0;
}

static ulong get_kdf_duration(ulong iterations) {
	static const char *msg = "123456789abcdef0";
	const size_t msg_len = strlen(msg);
	static const char *salt = "saltsalt";
	const size_t salt_len = strlen(salt);
	unsigned char key[16];
	struct timespec start_time, end_time;

	clock_gettime(CLOCK_MONOTONIC, &start_time);

	if (gcry_kdf_derive(msg, msg_len, GCRY_KDF_ITERSALTED_S2K, GCRY_MD_SHA1, salt, salt_len, iterations, sizeof(key), key)) {
		fprintf(stderr, "fatal: error in gcry_kdf_derive()\n");
		exit(255);
	}

	clock_gettime(CLOCK_MONOTONIC, &end_time);

	return timediff_msecs(&start_time, &end_time);
}

ulong get_s2k_count(void) {
	init_libgcrypt();

	ulong count = 65536, msecs = 100;

	while(count) {
		msecs = get_kdf_duration(count);
#ifdef DEBUG
		printf("TD iterations: %8lu   msecs: %lu\n", count, msecs);
#endif
		if (msecs > 100) {
			break;
		}
		count *= 2;
	}
	count = (ulong)(((double)count / msecs) * 100UL);
	/* round to multple of 1024 */
	count /= 1024UL;
	count *= 1024UL;
	if (count < 65536) {
		count = 65536;
	}
	return count;
}

#ifdef MAIN

int main(void) {
	printf("%lu\n", get_s2k_count());
	return 0;
}

#endif	/* MAIN */

/* EOB */

