#
#   test_saracrypt.py   WJ118
#   Copyright 2018 SURFsara B.V.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''saracrypt unit tests'''

import unittest
import os
import tempfile
import subprocess

import saracrypt


class TestMetric(unittest.TestCase):
    '''tests for class Metric'''

    def test_multiplier(self):
        '''test staticmethod Metric.multiplier(s)'''

        self.assertEqual(saracrypt.Metric.multiplier('B'), 1)
        self.assertEqual(saracrypt.Metric.multiplier('kB'), 1000)
        self.assertEqual(saracrypt.Metric.multiplier('MB'), 10**6)
        self.assertEqual(saracrypt.Metric.multiplier('GB'), 10**9)
        self.assertEqual(saracrypt.Metric.multiplier('TB'), 10**12)
        self.assertEqual(saracrypt.Metric.multiplier('PB'), 10**15)
        self.assertEqual(saracrypt.Metric.multiplier('EB'), 10**18)
        self.assertEqual(saracrypt.Metric.multiplier('ZB'), 10**21)
        self.assertEqual(saracrypt.Metric.multiplier('YB'), 10**24)
        self.assertEqual(saracrypt.Metric.multiplier('kiB'), 1024)
        self.assertEqual(saracrypt.Metric.multiplier('MiB'), 1024*1024)
        self.assertEqual(saracrypt.Metric.multiplier('GiB'), 1024**3)
        self.assertEqual(saracrypt.Metric.multiplier('TiB'), 1024**4)
        # case in-sensitivity
        self.assertEqual(saracrypt.Metric.multiplier('KB'), 1000)
        self.assertEqual(saracrypt.Metric.multiplier('KIB'), 1024)
        # even without bytes
        self.assertEqual(saracrypt.Metric.multiplier('K'), 1000)
        # ValueError exception
        self.assertRaises(ValueError, saracrypt.Metric.multiplier, 'QB')
        # return type
        self.assertIsInstance(saracrypt.Metric.multiplier('K'), int)

    def test_parse(self):
        '''test method Metric.parse(s)'''

        m = saracrypt.Metric()
        self.assertEqual(m.parse('128 B'), 128)
        self.assertEqual(m.parse('40 kB'), 40 * 1000)
        self.assertEqual(m.parse('4 MiB'), 4 * 1024 * 1024)
        # ValueError exception
        self.assertRaises(ValueError, m.parse, '')
        # bytes can not be a float, but larger values can be
        self.assertRaises(ValueError, m.parse, '1.10')
        self.assertEqual(m.parse('1.10 MB'), 1100000)
        self.assertRaises(ValueError, m.parse, '1 2 3')
        self.assertRaises(ValueError, m.parse, '1 MiB kiB')

    def test_format(self):
        '''test method Metric.format(s)'''

        # Note: .format() also implicitly tests .prefix()
        # and .__str__()

        m = saracrypt.Metric(1000)
        self.assertEqual(m.format(), '1.0 kB')
        self.assertEqual(str(m), '1.0 kB')
        m = saracrypt.Metric(1024)
        self.assertEqual(m.format(1024), '1.0 kiB')
        self.assertRaises(RuntimeError, m.format, factor=256)



class TestProgressBar(unittest.TestCase):
    '''tests for class ProgressBar'''

    def test_progress(self):
        '''test the progressbar'''

        # bar.update() already prints the progress bar
        # so to make output nicer, print some newlines
        print()
        b = saracrypt.ProgressBar(max_value=100)
        self.assertEqual(str(b), '|               |   0% ')
        # trick the time dependent code in .update()
        b._lastupdate = -1000   # pylint: disable=W0212
        b.update(20)
        print()
        self.assertEqual(str(b), '|===            |  20% ')
        b._lastupdate = -1000   # pylint: disable=W0212
        b.update(50)
        print()
        self.assertEqual(str(b), '|========       |  50% ')
        b._lastupdate = -1000   # pylint: disable=W0212
        b.update(80)
        print()
        self.assertEqual(str(b), '|============   |  80% ')
        b._lastupdate = -1000   # pylint: disable=W0212
        b.update(100)
        print()
        self.assertEqual(str(b), '|===============| 100% ')
        b._lastupdate = -1000   # pylint: disable=W0212
        b.update(120)
        self.assertEqual(str(b), '|===============| 100% ')



class TestMyDirEntry(unittest.TestCase):
    '''tests for class MyDirEntry'''

    def test_is_dir(self):
        '''test method is_dir()'''

        d = saracrypt.MyDirEntry(None, path='/etc')
        self.assertTrue(d.is_dir())
        self.assertTrue(d.is_dir(follow_symlinks=True))

        d = saracrypt.MyDirEntry(None, path='test_saracrypt.py')
        self.assertFalse(d.is_dir())

        d = saracrypt.MyDirEntry(None, path='/orugdkkh-larhk-lrhae-lrhkea-lhkea')
        self.assertFalse(d.is_dir())

    def test_is_file(self):
        '''test method is_file()'''

        d = saracrypt.MyDirEntry(None, path='test_saracrypt.py')
        self.assertTrue(d.is_file())

        d = saracrypt.MyDirEntry(None, path='/etc')
        self.assertFalse(d.is_file())

        d = saracrypt.MyDirEntry(None, path='/orugdkkh-larhk-lrhae-lrhkea-lhkea')
        self.assertFalse(d.is_file())

    def test_is_symlink(self):
        '''test method is_symlink()'''

        # make a symlink under /tmp/
        filename = '/tmp/test_saracrypt.{}'.format(os.getpid())
        # symlink to non-existing file
        try:
            os.symlink('/tmp/test_saracrypt.blballoretru', filename)
        except OSError as err:
            print('warning: failed to create test symlink; {}'.format(err.strerror))
            return

        d = saracrypt.MyDirEntry(None, path=filename)
        self.assertTrue(d.is_symlink())
        self.assertFalse(d.is_file())
        self.assertFalse(d.is_dir())

        # cleanup
        try:
            os.unlink(filename)
        except OSError:
            # ignore (probably: no such file)
            pass

        # symlink to existing file
        try:
            os.symlink('/etc/group', filename)
        except OSError as err:
            print('warning: failed to create test symlink; {}'.format(err.strerror))
            return

        d = saracrypt.MyDirEntry(None, path=filename)
        self.assertTrue(d.is_symlink())
        self.assertTrue(d.is_file())
        self.assertFalse(d.is_file(follow_symlinks=False))
        self.assertFalse(d.is_dir())

        # cleanup
        try:
            os.unlink(filename)
        except OSError:
            # ignore (probably: no such file)
            pass

        # symlink to existing dir
        try:
            os.symlink('/etc', filename)
        except OSError as err:
            print('warning: failed to create test symlink; {}'.format(err.strerror))
            return

        d = saracrypt.MyDirEntry(None, path=filename)
        self.assertTrue(d.is_symlink())
        self.assertFalse(d.is_file())
        self.assertFalse(d.is_file(follow_symlinks=False))
        self.assertTrue(d.is_dir())
        self.assertFalse(d.is_dir(follow_symlinks=False))

        # cleanup
        try:
            os.unlink(filename)
        except OSError:
            # ignore (probably: no such file)
            pass

        d = saracrypt.MyDirEntry(None, path='/orugdkkh-larhk-lrhae-lrhkea-lhkea')
        self.assertFalse(d.is_symlink())

    def test_inode(self):
        '''test method inode()'''

        d = saracrypt.MyDirEntry(None, path='/orugdkkh-larhk-lrhae-lrhkea-lhkea')
        self.assertRaises(FileNotFoundError, d.inode)

        d = saracrypt.MyDirEntry(None, path='test_saracrypt.py')
        self.assertTrue(d.inode() > 0)

    def test_init(self):
        '''test the __init__() with a DirEntry object'''

        for entry in os.scandir('.'):
            # make a MyDirEntry from a os.DirEntry instance
            d = saracrypt.MyDirEntry(entry)
            try:
                stats = d.stat(follow_symlinks=False)
            except OSError as err:
                # maybe Permission denied, or even File not found
                print('warning: {}: {}'.format(entry.name, err.strerror))
            else:
                self.assertIsInstance(stats, os.stat_result)



class TestBatchPath(unittest.TestCase):
    '''tests for class BatchPath'''

    def test_init(self):
        '''test the __init__()'''

        bp = saracrypt.BatchPath('/some/path', 'subdir/filename')
        self.assertIsInstance(bp, saracrypt.BatchPath)

        bp = saracrypt.BatchPath('some/path', 'subdir/filename')
        self.assertIsInstance(bp, saracrypt.BatchPath)

        bp = saracrypt.BatchPath('whatever', 'whatever')
        self.assertIsInstance(bp, saracrypt.BatchPath)

        # should it raise ValueError?
        self.assertRaises(AssertionError, saracrypt.BatchPath, '/', '/invalid/relpath')

        # this is a bit weird test, because it doesn't assert directly hardcoded behavior
        # it is good however that this invalid condition would raise an exception
        self.assertRaises(IndexError, saracrypt.BatchPath, '/', '')

        bp = saracrypt.BatchPath('', 'whatever')
        self.assertIsInstance(bp, saracrypt.BatchPath)

    def test_string(self):
        '''test the __str__() method'''

        bp = saracrypt.BatchPath('/some/path', 'subdir/filename')
        self.assertEqual(str(bp), '/some/path/subdir/filename')

    def test_destpath(self):
        '''test the .destpath() method'''

        bp = saracrypt.BatchPath('/some/path', 'subdir/filename')
        self.assertEqual(bp.destpath('/dest'), '/dest/subdir/filename')
        self.assertEqual(bp.destpath('/'), '/subdir/filename')
        self.assertEqual(bp.destpath('dest'), 'dest/subdir/filename')
        self.assertEqual(bp.destpath(''), 'subdir/filename')



class TestBatchError(unittest.TestCase):
    '''test class BatchError'''

    def test_len(self):
        '''test the len() of a BatchError

        This also implicitly tests .add(), .extend(),
        and operator += (iadd) and operator + (add)
        '''

        berr = saracrypt.BatchError()
        self.assertEqual(len(berr), 0)
        berr.add('a', 'msg1')
        berr.add('b', 'msg2')
        berr.add('c', 'msg3')
        self.assertEqual(len(berr), 3)
        berr.extend([('a', 'msg1'), ('b', 'msg2'), ('c', 'msg3')])
        self.assertEqual(len(berr), 6)

        berr2 = saracrypt.BatchError()
        berr2.add('a1', 'msg1')
        berr2.add('a2', 'msg2')
        berr += berr2
        self.assertEqual(len(berr), 8)

        berr3 = berr + berr2
        self.assertEqual(len(berr3), 10)
        self.assertEqual(len(berr), 8)
        self.assertEqual(len(berr2), 2)

    def test_items(self):
        '''test method .items()'''

        berr = saracrypt.BatchError()
        self.assertEqual(len(berr), 0)
        berr.add('a', 'msg1')
        berr.add('b', 'msg2')
        berr.add('c', 'msg3')
        self.assertEqual(berr.items(), [('a', 'msg1'), ('b', 'msg2'), ('c', 'msg3')])

        self.assertIsInstance(berr.items(), type([('a', 'b'),]))


class TestAppStats(unittest.TestCase):
    '''test class AppStats'''

    def test_add(self):
        '''test adding up AppStats instances'''

        a1 = saracrypt.AppStats()
        self.assertEqual(a1.filecount, 0)
        self.assertEqual(a1.errcount, 0)
        self.assertEqual(a1.size, 0)

        a2 = saracrypt.AppStats()
        # we assign values directly into AppStats, which is OK
        a2.filecount = 1
        a2.errcount = 2
        a2.size = 100

        a1 += a2
        self.assertEqual(a1.filecount, 1)
        self.assertEqual(a1.errcount, 2)
        self.assertEqual(a1.size, 100)
        self.assertEqual(a2.filecount, 1)
        self.assertEqual(a2.errcount, 2)
        self.assertEqual(a2.size, 100)

        a1 += a2
        self.assertEqual(a1.filecount, 2)
        self.assertEqual(a1.errcount, 4)
        self.assertEqual(a1.size, 200)
        self.assertEqual(a2.filecount, 1)
        self.assertEqual(a2.errcount, 2)
        self.assertEqual(a2.size, 100)



class TestBatch(unittest.TestCase):
    '''test class Batch'''

    def test_add_many(self):
        '''method add() should raise BatchFullError when many files are added'''

        def add_many(b):
            '''local func to add many files to a batch'''

            for _i in range(saracrypt.Batch.MAXCOUNT + 1):
                b.add('srcdir', 'srcdir/a', 1)

        b = saracrypt.Batch()
        self.assertRaises(saracrypt.BatchFullError, add_many, b)

    def test_add_big(self):
        '''method add() should raise BatchFullError when size becomes too large'''

        def add_big(b):
            '''local func to add many files to a batch'''

            size = int(saracrypt.Batch.MAXSIZE // 8)
            for _i in range(10):
                b.add('srcdir', 'srcdir/a', size)

        b = saracrypt.Batch()
        self.assertRaises(saracrypt.BatchFullError, add_big, b)

    def test_len(self):
        '''test len(Batch)'''

        b = saracrypt.Batch()
        self.assertEqual(len(b), 0)
        b.add('srcdir', 'srcdir/a', 1)
        b.add('srcdir', 'srcdir/b', 1)
        b.add('srcdir', 'srcdir/c', 1)
        self.assertEqual(len(b), 3)

    def test_remove(self):
        '''test method remove()'''

        b = saracrypt.Batch()
        b.add('srcdir', 'srcdir/a', 1)
        b.add('srcdir', 'srcdir/b', 1)
        b.add('srcdir', 'srcdir/c', 1)
        self.assertEqual(len(b), 3)

        b.remove('srcdir/b')
        found = False
        for x in b.filelist:
            if str(x) == 'srcdir/b':
                found = True
                break
        self.assertFalse(found)
        self.assertEqual(len(b), 2)

    def test_have_dmf(self):
        '''tests whether HAVE_DMF works'''

        # constructing an instance should initialize HAVE_DMF
        _b = saracrypt.Batch()
        # this is a crappy test ... but should work fine in general
        self.assertEqual(saracrypt.Batch.HAVE_DMF, os.path.isfile('/usr/bin/dmls'))


class TestFuncs(unittest.TestCase):
    '''test a bunch of functions'''

    def test_is_exec(self):
        '''test is_exec()'''

        self.assertTrue(saracrypt.is_exec('/bin/ls'))
        self.assertFalse(saracrypt.is_exec('/etc/profile'))
        self.assertFalse(saracrypt.is_exec('ehaesashsrtrLRL-RHNCH'))

    def test_is_opendir(self):
        '''test is_opendir()'''

        self.assertTrue(saracrypt.is_opendir('/'))
        self.assertTrue(saracrypt.is_opendir('/tmp'))
        with tempfile.TemporaryDirectory() as tmpdir:
            try:
                self.assertFalse(saracrypt.is_opendir(str(tmpdir)))
            except OSError as err:
                print('warning: {}: {}'.format(err.filename, err.strerror))
        self.assertRaises(OSError, saracrypt.is_opendir, 'uthtehuothuontantan')

    def test_split_cmd(self):
        '''test split_cmd()'''

        a = saracrypt.split_cmd('ls -l')
        self.assertEqual(a, ['ls', '-l'])
        a = saracrypt.split_cmd('ls -l $DIR', {'$DIR': 'aap'})
        self.assertEqual(a, ['ls', '-l', 'aap'])
        a = saracrypt.split_cmd('ls -l $DIR', {'$DIR': 'aap', '$DIR2': 'noot'})
        self.assertEqual(a, ['ls', '-l', 'aap'])
        # missing key:value
        # should it raise ValueError?
        self.assertRaises(AssertionError, saracrypt.split_cmd, 'ls -l $DIR', {})
        # I don't care so much for the following test case
        # but this is how it currently behaves
        a = saracrypt.split_cmd('ls -l $DIR', None)
        self.assertEqual(a, ['ls', '-l', '$DIR'])

    def test_load_password(self):
        '''test load_password()'''

        # create empty temp file
        f = tempfile.NamedTemporaryFile(delete=False)
        self.assertRaises(ValueError, saracrypt.load_password, f.name)

        # write just empty lines
        f.write(b'\n\n\n\n\n')
        f.close()
        self.assertRaises(ValueError, saracrypt.load_password, f.name)

        # cleanup
        try:
            os.unlink(f.name)
        except OSError:
            pass

        f = tempfile.NamedTemporaryFile(delete=False)
        f.write(b'this is my test\n')
        f.close()

        loaded_passwd = saracrypt.load_password(f.name)
        self.assertEqual(loaded_passwd, 'this is my test')

        # cleanup
        try:
            os.unlink(f.name)
        except OSError:
            pass

    def test_is_excluded(self):
        '''test is_excluded()'''

        self.assertFalse(saracrypt.is_excluded('a', []))
        self.assertFalse(saracrypt.is_excluded('a', ['*.dat',]))
        self.assertTrue(saracrypt.is_excluded('a.dat', ['*.dat',]))
        self.assertTrue(saracrypt.is_excluded('a.dat', ['*.mp3', '*.dat']))
        self.assertTrue(saracrypt.is_excluded('a.dat', ['*.mp3', '*.bin', '?.dat']))
        self.assertTrue(saracrypt.is_excluded('a.dat', ['*.mp3', '*.bin', 'a*']))
        self.assertTrue(saracrypt.is_excluded('a', ['a',]))

    def test_check_srcpath(self):
        '''test check_srcpath()'''

        self.assertRaises(ValueError, saracrypt.check_srcpath, '')
        self.assertEqual(saracrypt.check_srcpath('/'), '/')
        # leading double slashes may be shares on certain OSes ...
        self.assertEqual(saracrypt.check_srcpath('//'), '//')
        self.assertEqual(saracrypt.check_srcpath('///'), '/')
        self.assertEqual(saracrypt.check_srcpath('////'), '/')
        self.assertEqual(saracrypt.check_srcpath('/etc'), '/etc')
        self.assertEqual(saracrypt.check_srcpath('/etc/'), '/etc')
        self.assertEqual(saracrypt.check_srcpath('/etc//'), '/etc')
        self.assertEqual(saracrypt.check_srcpath('/etc//profile'), '/etc/profile')

    def test_check_file(self):
        '''test check_file()'''

        self.assertRaises(ValueError, saracrypt.check_file, 'bablabarocoott')
        self.assertRaises(ValueError, saracrypt.check_file, '/')
        # and not raises ...
        try:
            saracrypt.check_file('test_saracrypt.py')
        except ValueError:
            self.fail('check_file() raised an unexpected ValueError')

    def test_check_dir(self):
        '''test check_dir()'''

        self.assertRaises(ValueError, saracrypt.check_dir, 'bablabarocoott')
        self.assertRaises(ValueError, saracrypt.check_dir, 'test_saracrypt.py')
        # and not raises ...
        try:
            saracrypt.check_dir('/tmp')
        except ValueError:
            self.fail('check_file() raised an unexpected ValueError')

    def test_rscandir(self):
        '''test rscandir()'''

        # rscandir() should produce the same as 'find -type f'
        cmd_arr = 'find . -type f -print0'.split()
        proc = subprocess.run(cmd_arr, stdout=subprocess.PIPE)
        line = proc.stdout.decode('utf-8')
        find_set = set(x for x in line.split('\x00') if x)

        rscandir_set = set([])
        for x in saracrypt.rscandir('.', [], []):
            rscandir_set.add(x.path)

        # this is rather bold ... but it should work
        # if the filesystem is a bit quiet
        self.assertEqual(find_set, rscandir_set)



class TestBatchCrypt(unittest.TestCase):
    '''test class BatchCrypt'''

    def test_remove_already_crypted(self):
        '''test method remove_already_crypted()'''

        b = saracrypt.BatchCrypt([saracrypt.BatchPath('src', 'a'),
                                  saracrypt.BatchPath('src', 'b.gpg'),
                                  saracrypt.BatchPath('src', 'c')])
        self.assertEqual(len(b), 3)
        b.remove_already_crypted(decrypt=False)
        self.assertEqual(len(b), 2)
        found = False
        for x in b.filelist:
            if str(x).endswith('.gpg'):
                found = True
        self.assertFalse(found)

        # reverse the test
        b = saracrypt.BatchCrypt([saracrypt.BatchPath('src', 'a.gpg'),
                                  saracrypt.BatchPath('src', 'b'),
                                  saracrypt.BatchPath('src', 'c.gpg')])
        self.assertEqual(len(b), 3)
        b.remove_already_crypted(decrypt=True)
        self.assertEqual(len(b), 2)
        found = False
        for x in b.filelist:
            if not str(x).endswith('.gpg'):
                found = True
        self.assertFalse(found)

    def test_already_crypted(self):
        '''test staticmethod already_crypted()'''

        self.assertTrue(saracrypt.BatchCrypt.already_crypted('a.gpg', False))
        self.assertFalse(saracrypt.BatchCrypt.already_crypted('a.gpg', True))
        self.assertTrue(saracrypt.BatchCrypt.already_crypted('a', True))
        self.assertFalse(saracrypt.BatchCrypt.already_crypted('a', False))

    def test_make_output_batchpath(self):
        '''test staticmethod make_output_batchpath()'''

        bp = saracrypt.BatchPath('src', 'a')
        out_filename = str(saracrypt.BatchCrypt.make_output_batchpath(bp, None))
        self.assertEqual(out_filename, 'src/a.gpg')

        out_filename = str(saracrypt.BatchCrypt.make_output_batchpath(bp, 'dest/dir'))
        self.assertEqual(out_filename, 'dest/dir/a.gpg')

        bp = saracrypt.BatchPath('src', 'a.gpg')
        out_filename = str(saracrypt.BatchCrypt.make_output_batchpath(bp, None, True))
        self.assertEqual(out_filename, 'src/a')

        out_filename = str(saracrypt.BatchCrypt.make_output_batchpath(bp, 'dest/dir', True))
        self.assertEqual(out_filename, 'dest/dir/a')



if __name__ == '__main__':
    unittest.main()

# EOB
