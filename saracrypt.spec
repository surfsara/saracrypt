#
#   saracrypt.spec
#   Copyright 2018 SURFsara B.V.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

Name:           saracrypt
Version:        %{tag_version}
Release:        %{tag_release}
License:        Apache
Summary:        bulk file encryption tool
Url:            https://gitlab.com/surfsara/saracrypt/
Group:          Utilities
BuildRequires:  python3 bash make swig gcc python3-devel libgcrypt-devel libgpg-error-devel
Requires:       python3 gpg libgcrypt
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
Encrypt large datasets in bulk.

%prep
cd %{_sourcedir}
###%setup -D -T
# make a setup.conf file
%if 0%{?rhel}
%define setup_conf setup.conf.rhel
%else
%define setup_conf setup.conf
%endif
sed "s?^BUILDROOT=?BUILDROOT=%{buildroot}?" %{setup_conf} >setup.conf.specbuild

%install
%{__rm} -rf %{buildroot}
# create system dirs
cd %{_sourcedir}
. ./setup.conf.specbuild
%{__install} -m 755 -d %{buildroot}
%{__install} -m 755 -d "$PREFIX"
%{__install} -m 755 -d "$BINDIR"
%{__install} -m 755 -d "$LIBDIR"
%{__install} -m 755 -d "$MANDIR"
%{__install} -m 755 -d "$MANDIR/man1"
%{__install} -m 755 -d "$DOCDIR"
SETUP_CONF=setup.conf.specbuild ./setup.sh

%files
%defattr(-, root, root)
/usr/local/bin/
/usr/local/lib64/
/usr/share/

%changelog

