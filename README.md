saracrypt
=========

File encryption tool. It uses GnuPG under the hood.

```
usage: saracrypt [-m KEY] [-d] [options] FILE|DIR [..]

  -h, --help               show this help message and exit
  -d, --decrypt            decrypt encrypted files
  -p, --progress           show progress bar
  -m, --master=KEYFILE     use masterkey file for encrypt/decrypt
  -g, --genkey=KEYFILE     generate new masterkey
      --change=KEYFILE     change passphrase on masterkey file
  -x, --exclude=PATTERN    exclude files matching PATTERN
  -X, --exclude-from=FILE  read exclude patterns from FILE
      --delete             delete source file after crypting
  -D, --destdir=DESTDIR    place output files under DESTDIR
  -b, --batch=FILE         read passphrase from FILE
      --bail               stop on error (early exit)
      --werror             all warnings and errors are fatal errors
      --stats              report statistics after crypting
      --cipher=CIPHER      set encryption cipher  (default: AES256)
      --gpg=PATH           use alternative GPG executable (default: gpg)
      --debug              show debug output
  -n, --dry-run            do not actually crypt or delete files
  -q, --quiet              suppress informational messages and warnings
      --version            show version info and exit

Ciphers: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
CAMELLIA128, CAMELLIA192, CAMELLIA256
```

Note: The list of ciphers may be longer or shorter depending on
the version of `gpg` you have installed.

`saracrypt` is DMF-aware and will use bulk-put-and-get when run
on SURFsara's data archive. This is not a requirement however, and
you are welcome to use `saracrypt` elsewhere.

See the userguide.md for extensive documentation on how to use `saracrypt`.


Installation
------------

Install by hand:

```
$ sudo ./setup.sh
```

Or you can build an RPM package by running (for example):

```
$ ./build_rpm.sh release-1.2
```

Notes:

* by default it installs to `/usr/local`
* use `SETUP_CONF=mysetup.conf ./setup.sh` if you need other settings
* python3 >= 3.6 is required
* the default `python3` binary is used. Set `PYTHON3` to point at
  an alternative python3 binary, or use `PATH`
* build depends on make, gcc, swig, python3-devel, libgcrypt-devel,
  libgpg-error-devel


gitlab CI/CD
------------

gitlab CI/CD is supported;

* tag `test` builds and runs unittest on multiple platforms
* tag `release-*` builds a .rpm file


Copyright notice and software license
-------------------------------------

Copyright 2018 SURFsara B.V.

You may use the `saracrypt` software under terms described in the Apache
License Version 2.0 ("the License").

* http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

