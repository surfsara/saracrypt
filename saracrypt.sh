#!/bin/bash
#
#	saracrypt startup wrapper
#   Copyright 2018 SURFsara B.V.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

# do not set PATH (because use of modules)
#PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

BINDIR=$(dirname $0)
if [[ -z $BINDIR ]]
then
	BINDIR="."
fi
PREFIX="${BINDIR%/bin}"
if [[ -d $PREFIX/lib64 ]]
then
	LIBDIR="$PREFIX/lib64"
else
	LIBDIR="$PREFIX/lib"
fi
PYTHONPATH="$LIBDIR/saracryptlib:$PYTHONPATH"
export PYTHONPATH

# run saracrypt
exec $BINDIR/saracrypt.py "$@"

# EOB
